FROM openjdk:16-jdk-alpine
ARG FILE_NAME=combacal-service-0.0.1-SNAPSHOT.jar
ARG JAR_FILE=build/libs/${FILE_NAME}
# environment variable with default value
ENV SPRING_PROFILE=prod

COPY ${JAR_FILE} /opt/lib/${FILE_NAME}

LABEL maintainer=UZH
WORKDIR /app
COPY build/libs /opt/libs/
COPY build/resources /opt/resources/
COPY build/classes /opt/classes/
ENTRYPOINT ["java", "-Dspring.profiles.active=${SPRING_PROFILE}", "-jar","/opt/lib/combacal-service-0.0.1-SNAPSHOT.jar"]
EXPOSE 8080
