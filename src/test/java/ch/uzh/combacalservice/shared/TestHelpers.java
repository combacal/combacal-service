package ch.uzh.combacalservice.shared;

import ch.uzh.combacalservice.model.medicine.*;
import ch.uzh.combacalservice.model.medicine.dto.*;
import ch.uzh.combacalservice.model.people.Caregiver;
import ch.uzh.combacalservice.model.people.Client;
import ch.uzh.combacalservice.model.people.Community;
import ch.uzh.combacalservice.model.people.Household;
import ch.uzh.combacalservice.model.people.dto.CaregiverDto;
import ch.uzh.combacalservice.model.people.dto.ClientDto;
import ch.uzh.combacalservice.model.people.dto.CommunityDto;
import ch.uzh.combacalservice.model.people.dto.HouseholdDto;
import ch.uzh.combacalservice.model.shared.*;
import ch.uzh.combacalservice.model.shared.dto.DistrictDto;
import ch.uzh.combacalservice.model.shared.dto.LocationDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.text.SimpleDateFormat;
import java.util.Collections;

public class TestHelpers {
    public static Caregiver createCaregiver() {
        Caregiver caregiver = new Caregiver();
        caregiver.setFirstName("Harry");
        caregiver.setLastName("Hirsch");
        caregiver.setGender(Gender.MALE);
        caregiver.setProfession(Profession.DOCTOR);

        caregiver.setDistrict(TestHelpers.createDistrict());
        caregiver.setCommunities(Collections.emptyList());

        return caregiver;
    }

    public static CaregiverDto createCaregiverDto() {
        CaregiverDto caregiverDto = new CaregiverDto();
        caregiverDto.setFirstName("Harry");
        caregiverDto.setLastName("Hirsch");
        caregiverDto.setGender(Gender.MALE);
        caregiverDto.setProfession(Profession.DOCTOR);

        caregiverDto.setDistrict(TestHelpers.createDistrictDto());
        caregiverDto.setCommunities(Collections.emptyList());

        return caregiverDto;
    }

    public static Client createClient() {
        Client client = new Client();
        client.setFirstName("John");
        client.setLastName("Doe");
        client.setGender(Gender.MALE);

        try {
            String date = "2000-01-01";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.DATE_FORMAT);
            client.setBirthDate(simpleDateFormat.parse(date));
        } catch (Exception e) {
            e.printStackTrace();
        }

        client.setParents(Collections.emptyList());
        client.setHousehold(createHousehold());
        client.setDiseases(Collections.emptyList());
        client.setMedicalDossier(createDossier());

        return client;
    }

    public static ClientDto createClientDto() {
        ClientDto clientDto = new ClientDto();
        clientDto.setFirstName("John");
        clientDto.setLastName("Doe");
        clientDto.setGender(Gender.MALE);

        try {
            String date = "2000-01-01";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.DATE_FORMAT);
            clientDto.setBirthDate(simpleDateFormat.parse(date));
        } catch (Exception e) {
            e.printStackTrace();
        }

        clientDto.setParents(Collections.emptyList());
        clientDto.setHousehold(createHouseholdDto());
        clientDto.setDiseases(Collections.emptyList());
        clientDto.setMedicalDossier(createDossierDto());

        return clientDto;
    }

    public static Community createCommunity() {
        Community community = new Community();
        community.setClients(Collections.emptyList());
        return community;
    }

    public static CommunityDto createCommunityDto() {
        CommunityDto communityDto = new CommunityDto();
        communityDto.setClients(Collections.emptyList());
        return communityDto;
    }

    public static Disease createDisease() {
        Disease disease = new Disease();
        disease.setName("Testitis");

        disease.setSymptoms(Collections.emptyList());
        disease.setTreatments(Collections.emptyList());

        return disease;
    }

    public static DiseaseDto createDiseaseDto() {
        DiseaseDto diseaseDto = new DiseaseDto();
        diseaseDto.setName("Testitis");

        diseaseDto.setSymptoms(Collections.emptyList());
        diseaseDto.setTreatments(Collections.emptyList());

        return diseaseDto;
    }

    public static District createDistrict() {
        District district = new District();
        district.setName("Test District");

        return district;
    }

    public static DistrictDto createDistrictDto() {
        DistrictDto districtDto = new DistrictDto();
        districtDto.setName("Test District");

        return districtDto;
    }

    public static Dossier createDossier() {
        Dossier dossier = new Dossier();

        dossier.setMedicalHistory(Collections.emptyList());

        return dossier;
    }

    public static DossierDto createDossierDto() {
        DossierDto dossierDto = new DossierDto();
        dossierDto.setMedicalHistory(Collections.emptyList());
        return dossierDto;
    }

    public static Household createHousehold() {
        Household household = new Household();
        household.setLastName("Doe");

        household.setDistrict(createDistrict());
        household.setLocation(createLocation());

        return household;
    }

    public static HouseholdDto createHouseholdDto() {
        HouseholdDto householdDto = new HouseholdDto();
        householdDto.setLastName("Doe");

        householdDto.setDistrict(createDistrictDto());
        householdDto.setLocation(createLocationDto());

        return householdDto;
    }

    public static Location createLocation() {
        Location location = new Location();
        location.setLatitude("47.414571");
        location.setLongitude("8.549112");
        return location;
    }

    public static LocationDto createLocationDto() {
        LocationDto locationDto = new LocationDto();
        locationDto.setLatitude("47.414571");
        locationDto.setLongitude("8.549112");
        return locationDto;
    }

    public static MedicalHistoryItem createMedicalHistoryItem() {
        MedicalHistoryItem medicalHistoryItem = new MedicalHistoryItem();
        try {
            String date = "01.01.2000";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
            medicalHistoryItem.setTreatmentDate(simpleDateFormat.parse(date));
            medicalHistoryItem.setDiseases(Collections.emptyList());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return medicalHistoryItem;
    }

    public static MedicalHistoryItemDto createMedicalHistoryItemDto() {
        MedicalHistoryItemDto medicalHistoryItemDto = new MedicalHistoryItemDto();
        try {
            String date = "01.01.2000";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
            medicalHistoryItemDto.setTreatmentDate(simpleDateFormat.parse(date));
            medicalHistoryItemDto.setDiseases(Collections.emptyList());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return medicalHistoryItemDto;
    }

    public static Symptom createSymptom() {
        Symptom symptom = new Symptom();
        symptom.setName("Running Test");
        return symptom;
    }

    public static SymptomDto createSymptomDto() {
        SymptomDto symptomDto = new SymptomDto();
        symptomDto.setName("Running Test");
        return symptomDto;
    }

    public static Treatment createTreatment() {
        Treatment treatment = new Treatment();
        treatment.setName("Teston 500mg");
        return treatment;
    }

    public static TreatmentDto createTreatmentDto() {
        TreatmentDto treatmentDto = new TreatmentDto();
        treatmentDto.setName("Teston 500mg");
        return treatmentDto;
    }

    public static <T> String convertObjectToJsonString(T object) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(object);
    }
}
