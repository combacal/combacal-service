package ch.uzh.combacalservice.controller;

import ch.uzh.combacalservice.exception.ResourceNotFoundException;
import ch.uzh.combacalservice.model.people.Caregiver;
import ch.uzh.combacalservice.model.people.Client;
import ch.uzh.combacalservice.model.people.Community;
import ch.uzh.combacalservice.model.people.dto.CaregiverDto;
import ch.uzh.combacalservice.model.people.dto.CommunityDto;
import ch.uzh.combacalservice.service.impl.CaregiverServiceImpl;
import ch.uzh.combacalservice.shared.TestHelpers;
import ch.uzh.combacalservice.util.mapper.MapStructMapperImpl;
import ch.uzh.combacalservice.utils.TestLogAppender;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static ch.uzh.combacalservice.shared.TestHelpers.convertObjectToJsonString;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class CaregiverControllerTest {

    @Mock
    private CaregiverServiceImpl caregiverServiceMock;

    @Spy
    private MapStructMapperImpl mapStructMapper;

    @SpyBean
    @InjectMocks
    private CaregiverController controller;

    private MockMvc mockMvc;

    private TestLogAppender<CaregiverController> testLogAppender;

    private Caregiver caregiver;
    private Long caregiverId;
    private CaregiverDto caregiverDto;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        testLogAppender = new TestLogAppender<>(controller);

        caregiver = TestHelpers.createCaregiver();
        caregiverId = 42L;
        caregiverDto = TestHelpers.createCaregiverDto();
    }

    @Test
    public void getAllCaregivers() throws Exception {
        // arrange
        List<Caregiver> caregivers = Arrays.asList(caregiver, TestHelpers.createCaregiver());
        List<CaregiverDto> caregiverDtos = Arrays.asList(caregiverDto, TestHelpers.createCaregiverDto());
        when(caregiverServiceMock.getAllCaregivers()).thenReturn(caregivers);

        // act, assert
        mockMvc.perform(get("/caregivers"))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(content().json(convertObjectToJsonString(caregiverDtos)))
                .andExpect(status().isOk());

        testLogAppender.assertLoggedLast("GET: query all caregivers");
        verify(caregiverServiceMock, times(1)).getAllCaregivers();
    }

    @Test
    public void getCaregiver_returnsCaregiverWhenFound() throws Exception {
        // arrange
        when(caregiverServiceMock.findCaregiverById(anyLong())).thenReturn(caregiver);

        // act, assert
        mockMvc.perform(get(String.format("/caregivers/%d", caregiverId)))
                .andExpect(content().json(convertObjectToJsonString(caregiverDto)))
                .andExpect(status().isOk());

        testLogAppender.assertLoggedLast("GET: query caregiver 42");
        verify(caregiverServiceMock, times(1)).findCaregiverById(caregiverId);
    }

    @Test
    public void getCaregiver_throws404WhenNotFound() throws Exception {
        // arrange
        when(caregiverServiceMock.findCaregiverById(anyLong())).thenReturn(null);

        // act, assert
        mockMvc.perform(get(String.format("/caregivers/%d", caregiverId)))
                .andExpect(jsonPath("$").doesNotExist())
                .andExpect(status().isNotFound())
                .andExpect(result -> {
                    Exception exception = result.getResolvedException();
                    assertTrue(exception instanceof ResourceNotFoundException);
                    assertEquals("Resource of type Caregiver not found with id : '42'", result.getResolvedException().getMessage());
                });
        // assert
        testLogAppender.assertLoggedLast("GET: query caregiver 42");
        verify(caregiverServiceMock, times(1)).findCaregiverById(caregiverId);
    }

    @Test
    public void createCaregiver_returnsNewCaregiver() throws Exception {
        // arrange
        caregiver.setId(caregiverId);
        when(caregiverServiceMock.createCaregiver(any(Caregiver.class))).thenReturn(caregiver);

        // act, assert
        mockMvc.perform(post("/caregivers")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(convertObjectToJsonString(caregiverDto))
                )
                .andExpect(content().json(convertObjectToJsonString(caregiverDto)))
                .andExpect(status().isCreated());

        //testLogAppender.assertLoggedLast("POST: new caregiver created with id 42");
        //caregiver.setId(null);
        //verify(caregiverServiceMock, times(1)).createCaregiver(caregiver);
    }

    @Test
    public void updateCaregiver_returnsUpdatedCaregiverOnSuccess() throws Exception {
        // arrange
        caregiver.setId(caregiverId);
        String updatedLastName = "Hengst";
        caregiver.setLastName(updatedLastName);
        caregiverDto.setLastName(updatedLastName);
        when(caregiverServiceMock.updateCaregiver(any(Caregiver.class))).thenReturn(caregiver);

        // act, assert
        mockMvc.perform(put(String.format("/caregivers/%d", caregiverId))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(convertObjectToJsonString(caregiver))
                )
                .andExpect(content().json(convertObjectToJsonString(caregiverDto)))
                .andExpect(jsonPath("$.lastName").value(updatedLastName))
                .andExpect(status().isOk());

        testLogAppender.assertLoggedLast("PUT: updating caregiver 42");
        verify(caregiverServiceMock, times(1)).updateCaregiver(caregiver);
    }

    @Test
    public void updateCaregiver_throws404WhenNotExisting() throws Exception {
        // arrange
        caregiver.setId(caregiverId);
        when(caregiverServiceMock.updateCaregiver(any(Caregiver.class))).thenReturn(null);

        // act, assert
        mockMvc.perform(put(String.format("/caregivers/%d", caregiverId))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(convertObjectToJsonString(caregiverDto))
                )
                .andExpect(jsonPath("$").doesNotExist())
                .andExpect(status().isNotFound())
                .andExpect(result -> {
                    Exception exception = result.getResolvedException();
                    assertTrue(exception instanceof ResourceNotFoundException);
                    assertEquals("Resource of type Caregiver not found with id : '42'", exception.getMessage());
                });

        testLogAppender.assertLoggedLast("PUT: updating caregiver 42");
        verify(caregiverServiceMock, times(1)).updateCaregiver(caregiver);
    }

    @Test
    public void deleteCaregiver() {
        // arrange
        Long caregiverId = 42L;

        // act
        ResponseEntity<String> response = controller.deleteCaregiver(caregiverId);

        // assert
        testLogAppender.assertLoggedLast("deleting caregiver 42");
        assertEquals("deleting caregiver 42", response.getBody());
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    public void getCommunitiesForCaregiver_returnsCommunitiesWhenFound() throws Exception {
        // arrange
        List<Community> communities = Arrays.asList(TestHelpers.createCommunity(), TestHelpers.createCommunity());
        List<CommunityDto> communityDtos = Arrays.asList(TestHelpers.createCommunityDto(), TestHelpers.createCommunityDto());
        caregiver.setCommunities(communities);
        caregiverDto.setCommunities(communityDtos);
        when(caregiverServiceMock.findCaregiverById(anyLong())).thenReturn(caregiver);

        // act, assert
        mockMvc.perform(get(String.format("/caregivers/%d/communities", caregiverId)))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(content().json(convertObjectToJsonString(communityDtos)))
                .andExpect(status().isOk());

        testLogAppender.assertLoggedLast("GET: query communities for caregiver 42");
        verify(caregiverServiceMock, times(1)).findCaregiverById(caregiverId);
    }

    @Test
    public void getCommunitiesForCaregiver_throws404WhenNotFound() throws Exception {
        // arrange
        when(caregiverServiceMock.findCaregiverById(anyLong())).thenReturn(null);

        // act, assert
        mockMvc.perform(get(String.format("/caregivers/%d/communities", caregiverId)))
                .andExpect(jsonPath("$").doesNotExist())
                .andExpect(status().isNotFound())
                .andExpect(result -> {
                    Exception exception = result.getResolvedException();
                    assertTrue(exception instanceof ResourceNotFoundException);
                    assertEquals("Resource of type Caregiver not found with id : '42'", exception.getMessage());
                });

        testLogAppender.assertLoggedLast("GET: query communities for caregiver 42");
        verify(caregiverServiceMock, times(1)).findCaregiverById(caregiverId);
    }

    @Test
    public void getClientsForCaregiver_returnsClientsWhenFound() throws Exception {
        // arrange
        Client specificClientA = TestHelpers.createClient();
        specificClientA.setLastName("Stark");
        Client regularClientA = TestHelpers.createClient();
        Client specificClientB = TestHelpers.createClient();
        specificClientB.setLastName("Banner");
        List<Client> clientsA = Arrays.asList(regularClientA, specificClientA);
        List<Client> clientsB = Collections.singletonList(specificClientB);
        List<Community> communities = Arrays.asList(TestHelpers.createCommunity(), TestHelpers.createCommunity());
        communities.get(0).setClients(clientsA);
        communities.get(1).setClients(clientsB);
        caregiver.setCommunities(communities);
        when(caregiverServiceMock.findCaregiverById(anyLong())).thenReturn(caregiver);

        // act, assert
        mockMvc.perform(get(String.format("/caregivers/%d/clients", caregiverId)))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].lastName").value("Doe"))
                .andExpect(jsonPath("$[1].lastName").value("Stark"))
                .andExpect(jsonPath("$[2].lastName").value("Banner"))
                .andExpect(status().isOk());

        testLogAppender.assertLoggedLast("GET: query clients for caregiver 42");
        verify(caregiverServiceMock, times(1)).findCaregiverById(caregiverId);
    }

    @Test
    public void getClientsForCaregiver_throws404WhenNotFound() throws Exception {
        // arrange
        when(caregiverServiceMock.findCaregiverById(anyLong())).thenReturn(null);

        // act, assert
        mockMvc.perform(get(String.format("/caregivers/%d/clients", caregiverId)))
                .andExpect(jsonPath("$").doesNotExist())
                .andExpect(status().isNotFound())
                .andExpect(result -> {
                    Exception exception = result.getResolvedException();
                    assertTrue(exception instanceof ResourceNotFoundException);
                    assertEquals("Resource of type Caregiver not found with id : '42'", exception.getMessage());
                });

        testLogAppender.assertLoggedLast("GET: query clients for caregiver 42");
        verify(caregiverServiceMock, times(1)).findCaregiverById(caregiverId);
    }

    @Test
    public void getTrainingsForCaregiver() {
        // arrange
        Long caregiverId = 42L;

        // act
        ResponseEntity<String> response = controller.getTrainingsForCaregiver(caregiverId);

        // assert
        testLogAppender.assertLoggedLast("query trainings for caregiver 42");
        assertEquals("query trainings for caregiver 42", response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void getTasksForCaregiver() {
        // arrange
        Long caregiverId = 42L;

        // act
        ResponseEntity<String> response = controller.getTasksForCaregiver(caregiverId);

        // assert
        testLogAppender.assertLoggedLast("query tasks for caregiver 42");
        assertEquals("query tasks for caregiver 42", response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }
}
