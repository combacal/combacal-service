package ch.uzh.combacalservice.controller;

import ch.uzh.combacalservice.utils.TestLogAppender;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class TrainingControllerTest {

    private TrainingController controller;

    private TestLogAppender<TrainingController> testLogAppender;

    @BeforeEach
    public void init() {
        controller = new TrainingController();
        testLogAppender = new TestLogAppender<>(controller);
    }

    @Test
    public void getAllTrainings() {
        // act
        ResponseEntity<String> response = controller.getAllTrainings();

        // assert
        testLogAppender.assertLoggedLast("query all trainings");
        assertEquals("query all trainings", response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void getTraining() {
        // arrange
        Long trainingId = 42L;

        // act
        ResponseEntity<String> response = controller.getTraining(trainingId);

        // assert
        testLogAppender.assertLoggedLast("query training 42");
        assertEquals("query training 42", response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void getTrainingsForTopic() {
        // arrange
        String trainingTopic = "test topic";

        // act
        ResponseEntity<String> response = controller.getTrainingsForTopic(trainingTopic);

        // assert
        testLogAppender.assertLoggedLast("query trainings for topic 'test topic'");
        assertEquals("query trainings for topic 'test topic'", response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void createTraining() {
        // act
        ResponseEntity<String> response = controller.createTraining();

        // assert
        testLogAppender.assertLoggedLast("new training created");
        assertEquals("new training created", response.getBody());
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
    }

    @Test
    public void updateTraining() {
        // arrange
        Long trainingId = 42L;

        // act
        ResponseEntity<String> response = controller.updateTraining(trainingId);

        // assert
        testLogAppender.assertLoggedLast("updating training 42");
        assertEquals("updating training 42", response.getBody());
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    public void deleteTraining() {
        // arrange
        Long trainingId = 42L;

        // act
        ResponseEntity<String> response = controller.deleteTraining(trainingId);

        // assert
        testLogAppender.assertLoggedLast("deleting training 42");
        assertEquals("deleting training 42", response.getBody());
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }
}
