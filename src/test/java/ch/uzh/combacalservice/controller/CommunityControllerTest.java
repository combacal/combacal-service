package ch.uzh.combacalservice.controller;

import ch.uzh.combacalservice.exception.ResourceNotFoundException;
import ch.uzh.combacalservice.model.people.Client;
import ch.uzh.combacalservice.model.people.Community;
import ch.uzh.combacalservice.model.people.dto.ClientDto;
import ch.uzh.combacalservice.model.people.dto.CommunityDto;
import ch.uzh.combacalservice.service.impl.CommunityServiceImpl;
import ch.uzh.combacalservice.shared.TestHelpers;
import ch.uzh.combacalservice.util.mapper.MapStructMapperImpl;
import ch.uzh.combacalservice.utils.TestLogAppender;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static ch.uzh.combacalservice.shared.TestHelpers.convertObjectToJsonString;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


public class CommunityControllerTest {

    @Mock
    private CommunityServiceImpl communityServiceMock;

    @Spy
    private MapStructMapperImpl mapStructMapper;

    @SpyBean
    @InjectMocks
    private CommunityController controller;
    private MockMvc mockMvc;

    private TestLogAppender<CommunityController> testLogAppender;

    private Community community;
    private Long communityId;
    private CommunityDto communityDto;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        testLogAppender = new TestLogAppender<>(controller);

        community = TestHelpers.createCommunity();
        communityId = 42L;
        communityDto = TestHelpers.createCommunityDto();
    }

    @Test
    public void getAllCommunities() throws Exception {
        // arrange
        List<Community> communities = Arrays.asList(community, TestHelpers.createCommunity());
        List<CommunityDto> communityDtos = Arrays.asList(communityDto, TestHelpers.createCommunityDto());
        when(communityServiceMock.getAllCommunities()).thenReturn(communities);

        // act, assert
        mockMvc.perform(get("/communities"))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(content().json(convertObjectToJsonString(communityDtos)))
                .andExpect(status().isOk());

        testLogAppender.assertLoggedLast("GET: query all communities");
        verify(communityServiceMock, times(1)).getAllCommunities();
    }

    @Test
    public void getCommunity_returnsCommunityWhenFound() throws Exception {
        // arrange
        when(communityServiceMock.findCommunityById(anyLong())).thenReturn(community);

        // act, assert
        mockMvc.perform(get(String.format("/communities/%d", communityId)))
                .andExpect(content().json(convertObjectToJsonString(communityDto)))
                .andExpect(status().isOk());

        testLogAppender.assertLoggedLast("GET: query community 42");
        verify(communityServiceMock, times(1)).findCommunityById(communityId);
    }

    @Test
    public void getCommunity_throws404WhenNotFound() throws Exception {
        // arrange
        when(communityServiceMock.findCommunityById(anyLong())).thenReturn(null);

        // act, assert
        mockMvc.perform(get(String.format("/communities/%d", communityId)))
                .andExpect(jsonPath("$").doesNotExist())
                .andExpect(status().isNotFound())
                .andExpect(result -> {
                    Exception exception = result.getResolvedException();
                    assertTrue(exception instanceof ResourceNotFoundException);
                    assertEquals("Resource of type Community not found with id : '42'", result.getResolvedException().getMessage());
                });

        testLogAppender.assertLogged("GET: query community 42");
        verify(communityServiceMock, times(1)).findCommunityById(communityId);
    }

    @Test
    public void createCommunity_returnsNewCommunity() throws Exception {
        // arrange
        community.setId(communityId);
        when(communityServiceMock.createCommunity(any(Community.class))).thenReturn(community);

        // act, assert
        mockMvc.perform(post("/communities")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(convertObjectToJsonString(community)))
                .andExpect(content().json(convertObjectToJsonString(communityDto)))
                .andExpect(status().isCreated());

        testLogAppender.assertLoggedLast("POST: new community created with id 42");
        community.setId(null);
        verify(communityServiceMock, times(1)).createCommunity(community);
    }

    @Test
    public void updateCommunity_returnsUpdatedCommunityOnSuccess() throws Exception {
        // arrange
        List<Client> updatedClients = Collections.singletonList(TestHelpers.createClient());
        List<ClientDto> updatedClientDtos = Collections.singletonList(TestHelpers.createClientDto());
        community.setClients(updatedClients);
        communityDto.setClients(updatedClientDtos);
        when(communityServiceMock.updateCommunity(any(Community.class))).thenReturn(community);
        community.setId(communityId);

        // act, assert
        mockMvc.perform(put(String.format("/communities/%d", communityId))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(convertObjectToJsonString(communityDto))
                )
                .andDo(print())
                .andExpect(jsonPath("$.clients").isArray())
                .andExpect(jsonPath("$.clients", hasSize(1)))
                .andExpect(content().json(convertObjectToJsonString(communityDto)))
                .andExpect(status().isOk());

        testLogAppender.assertLoggedLast("PUT: updating community 42");
        verify(communityServiceMock, times(1)).updateCommunity(community);
    }

    @Test
    public void updateCommunity_throws404WhenNotExisting() throws Exception {
        // arrange
        community.setId(communityId);
        when(communityServiceMock.updateCommunity(any(Community.class))).thenReturn(null);

        // act, assert
        mockMvc.perform(put(String.format("/communities/%d", communityId))
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonString(communityDto))
        )
                .andExpect(jsonPath("$").doesNotExist())
                .andExpect(status().isNotFound())
                .andExpect(result -> {
                    Exception exception = result.getResolvedException();
                    assertTrue(exception instanceof ResourceNotFoundException);
                    assertEquals("Resource of type Community not found with id : '42'", exception.getMessage());
                });

        testLogAppender.assertLogged("PUT: updating community 42");
        verify(communityServiceMock, times(1)).updateCommunity(community);
    }

    @Test
    public void deleteCommunity() {
        // arrange
        Long communityId = 42L;

        // act
        ResponseEntity<String> response = controller.deleteCommunity(communityId);

        // assert
        testLogAppender.assertLoggedLast("DELETE: deleting community 42");
        assertEquals("DELETE: deleting community 42", response.getBody());
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }
}
