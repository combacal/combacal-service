package ch.uzh.combacalservice.controller;

import ch.qos.logback.classic.Level;
import ch.uzh.combacalservice.exception.ResourceNotFoundException;
import ch.uzh.combacalservice.model.people.Client;
import ch.uzh.combacalservice.model.people.dto.ClientDto;
import ch.uzh.combacalservice.service.impl.ClientServiceImpl;
import ch.uzh.combacalservice.shared.TestHelpers;
import ch.uzh.combacalservice.util.mapper.MapStructMapperImpl;
import ch.uzh.combacalservice.utils.TestLogAppender;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static ch.uzh.combacalservice.shared.TestHelpers.convertObjectToJsonString;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ClientControllerTest {

    @Mock
    private ClientServiceImpl clientServiceMock;

    @Spy
    private MapStructMapperImpl mapStructMapper;

    @SpyBean
    @InjectMocks
    private ClientController controller;

    private MockMvc mockMvc;

    private TestLogAppender<ClientController> testLogAppender;

    private Client client;
    private Long clientId;
    private ClientDto clientDto;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        testLogAppender = new TestLogAppender<>(controller);

        client = TestHelpers.createClient();
        clientId = 42L;
        clientDto = TestHelpers.createClientDto();
    }

    @Test
    public void getAllClients() throws Exception {
        // arrange
        List<Client> clients = Arrays.asList(client, TestHelpers.createClient());
        List<ClientDto> clientDtos = Arrays.asList(clientDto, TestHelpers.createClientDto());
        when(clientServiceMock.getAllClients()).thenReturn(clients);


        // act, assert
        mockMvc.perform(get("/clients"))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(content().json(convertObjectToJsonString(clientDtos)))
                .andExpect(status().isOk());

        testLogAppender.assertLoggedLast("GET: query all clients");
        verify(clientServiceMock, times(1)).getAllClients();
    }

    @Test
    public void getClient_returnsClientWhenFound() throws Exception {
        // arrange
        when(clientServiceMock.findClientById(anyLong())).thenReturn(client);

        // act, assert
        mockMvc.perform(get(String.format("/clients/%d", clientId)))
                .andExpect(content().json(convertObjectToJsonString(clientDto)))
                .andExpect(status().isOk());

        testLogAppender.assertLoggedLast("GET: query client 42");
        verify(clientServiceMock, times(1)).findClientById(clientId);
    }

    @Test
    public void getClient_throws404WhenNotFound() throws Exception {
        // arrange
        when(clientServiceMock.findClientById(anyLong())).thenReturn(null);

        // act, assert
        mockMvc.perform(get(String.format("/clients/%d", clientId)))
                .andExpect(jsonPath("$").doesNotExist())
                .andExpect(status().isNotFound())
                .andExpect(result -> {
                    Exception exception = result.getResolvedException();
                    assertTrue(exception instanceof ResourceNotFoundException);
                    assertEquals("Resource of type Client not found with id : '42'", result.getResolvedException().getMessage());
                });

        testLogAppender.assertLogged("GET: query client 42");
        verify(clientServiceMock, times(1)).findClientById(clientId);
    }

    @Test
    public void createClient_returnsNewClient() throws Exception {
        // arrange
        client.setId(clientId);
        when(clientServiceMock.createClient(any(Client.class))).thenReturn(client);

        // act, assert
        mockMvc.perform(post("/clients").contentType(MediaType.APPLICATION_JSON).content(convertObjectToJsonString(clientDto)))
                .andExpect(content().json(convertObjectToJsonString(clientDto)))
                .andExpect(status().isCreated());

        testLogAppender.assertLoggedLast("POST: new client created with id 42");
        client.setId(null);
        verify(clientServiceMock, times(1)).createClient(client);
    }

    @Test
    public void updateClient_returnsUpdatedClientOnSuccess() throws Exception {
        // arrange
        client.setId(null);
        String updatedLastName = "Developer";
        client.setLastName(updatedLastName);
        clientDto.setLastName(updatedLastName);
        when(clientServiceMock.updateClient(any(Client.class))).thenReturn(client);
        client.setId(clientId);

        // act, assert
        mockMvc.perform(put(String.format("/clients/%d", clientId))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(convertObjectToJsonString(clientDto))
                )
                .andExpect(content().json(convertObjectToJsonString(clientDto)))
                .andExpect(jsonPath("$.lastName").value(updatedLastName))
                .andExpect(status().isOk());

        testLogAppender.assertLoggedLast("PUT: updating client 42");
        verify(clientServiceMock, times(1)).updateClient(client);
    }

    @Test
    public void updateClient_throws404WhenNotExisting() throws Exception {
        // arrange
        client.setId(clientId);
        when(clientServiceMock.updateClient(any(Client.class))).thenReturn(null);

        // act, assert
        mockMvc.perform(put(String.format("/clients/%d", clientId))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(convertObjectToJsonString(clientDto))
                )
                .andExpect(jsonPath("$").doesNotExist())
                .andExpect(status().isNotFound())
                .andExpect(result -> {
                    Exception exception = result.getResolvedException();
                    assertTrue(exception instanceof ResourceNotFoundException);
                    assertEquals("Resource of type Client not found with id : '42'", exception.getMessage());
                });

        testLogAppender.assertLogged("PUT: updating client 42");
        verify(clientServiceMock, times(1)).updateClient(client);
    }

    @Test
    public void deleteClient_returnsNullOnSuccess() throws Exception {
        // arrange
        when(clientServiceMock.deleteClient(anyLong())).thenReturn(true);

        // act, assert
        mockMvc.perform(delete(String.format("/clients/%d", clientId)))
                .andExpect(jsonPath("$").doesNotExist())
                .andExpect(status().isNoContent());

        testLogAppender.assertLoggedLast("DELETE: deleting client 42");
        verify(clientServiceMock, times(1)).deleteClient(clientId);
    }

    @Test
    public void deleteClient_returnsPersistedClientOnFailure() throws Exception {
        // arrange
        when(clientServiceMock.deleteClient(anyLong())).thenReturn(false);
        when(clientServiceMock.findClientById(anyLong())).thenReturn(client);

        // act, assert
        mockMvc.perform(delete(String.format("/clients/%d", clientId)))
                .andExpect(content().json(convertObjectToJsonString(clientDto)))
                .andExpect(status().isInternalServerError());

        testLogAppender.assertLogged("DELETE: deleting client 42");
        testLogAppender.assertLoggedLast("Client with id 42 could not be deleted!", Level.ERROR);
        verify(clientServiceMock, times(1)).deleteClient(clientId);
    }
}
