package ch.uzh.combacalservice.controller;

import ch.qos.logback.classic.Level;
import ch.uzh.combacalservice.exception.ResourceNotFoundException;
import ch.uzh.combacalservice.model.people.Household;
import ch.uzh.combacalservice.model.people.dto.HouseholdDto;
import ch.uzh.combacalservice.model.shared.Location;
import ch.uzh.combacalservice.model.shared.dto.LocationDto;
import ch.uzh.combacalservice.service.impl.HouseholdServiceImpl;
import ch.uzh.combacalservice.shared.TestHelpers;
import ch.uzh.combacalservice.util.mapper.MapStructMapperImpl;
import ch.uzh.combacalservice.utils.TestLogAppender;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static ch.uzh.combacalservice.shared.TestHelpers.convertObjectToJsonString;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class HouseholdControllerTest {

    @Mock
    private HouseholdServiceImpl householdServiceMock;

    @Spy
    private MapStructMapperImpl mapStructMapper;

    @SpyBean
    @InjectMocks
    private HouseholdController controller;

    private MockMvc mockMvc;

    private TestLogAppender<HouseholdController> testLogAppender;

    private Household household;
    private Long householdId;
    private HouseholdDto householdDto;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        testLogAppender = new TestLogAppender<>(controller);

        household = TestHelpers.createHousehold();
        householdId = 42L;
        householdDto = TestHelpers.createHouseholdDto();
    }

    @Test
    public void getAllHouseholds() throws Exception {
        // arrange
        List<Household> households = Arrays.asList(household, TestHelpers.createHousehold());
        List<HouseholdDto> householdDtos = Arrays.asList(householdDto, TestHelpers.createHouseholdDto());
        when(householdServiceMock.getAllHouseholds()).thenReturn(households);


        // act, assert
        mockMvc.perform(get("/households"))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(content().json(convertObjectToJsonString(householdDtos)))
                .andExpect(status().isOk());
        testLogAppender.assertLoggedLast("GET: query all households");
        verify(householdServiceMock, times(1)).getAllHouseholds();
    }

    @Test
    public void getHousehold_returnsHouseholdWhenFound() throws Exception {
        // arrange
        when(householdServiceMock.findHouseholdById(anyLong())).thenReturn(household);

        // act, assert
        mockMvc.perform(get(String.format("/households/%d", householdId)))
                .andExpect(content().json(convertObjectToJsonString(householdDto)))
                .andExpect(status().isOk());

        testLogAppender.assertLoggedLast("GET: query household 42");
        verify(householdServiceMock, times(1)).findHouseholdById(householdId);
    }

    @Test
    public void getHousehold_throws404WhenNotFound() throws Exception {
        // arrange
        when(householdServiceMock.findHouseholdById(anyLong())).thenReturn(null);

        // act, assert
        mockMvc.perform(get(String.format("/households/%d", householdId)))
                .andExpect(jsonPath("$").doesNotExist()) // does not have a body
                .andExpect(status().isNotFound())
                .andExpect(result -> {
                    Exception exception = result.getResolvedException();
                    assertTrue(exception instanceof ResourceNotFoundException);
                    assertEquals("Resource of type Household not found with id : '42'", result.getResolvedException().getMessage());
                });

        testLogAppender.assertLogged("GET: query household 42");
        verify(householdServiceMock, times(1)).findHouseholdById(householdId);
    }

    @Test
    public void createHousehold_returnsNewHousehold() throws Exception {
        // arrange
        household.setId(householdId);
        when(householdServiceMock.createHousehold(any(Household.class))).thenReturn(household);

        // act, assert
        mockMvc.perform(post("/households")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonString(household)))
                .andExpect(content().json(convertObjectToJsonString(householdDto)))
                .andExpect(status().isCreated());

        testLogAppender.assertLoggedLast("POST: new household created with id 42");
        household.setId(null);
        verify(householdServiceMock, times(1)).createHousehold(household);
    }

    @Test
    public void updateHousehold_returnsUpdatedHouseholdOnSuccess() throws Exception {
        // arrange
        Location updatedLocation = new Location();
        LocationDto updatedLocationDto = new LocationDto();
        updatedLocation.setLatitude("123.45");
        updatedLocation.setLongitude("678.09");
        updatedLocationDto.setLatitude("123.45");
        updatedLocationDto.setLongitude("678.09");
        household.setLocation(updatedLocation);
        householdDto.setLocation(updatedLocationDto);
        when(householdServiceMock.updateHousehold(any(Household.class))).thenReturn(household);
        household.setId(householdId);

        // act, assert
        mockMvc.perform(put(String.format("/households/%d", householdId))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(convertObjectToJsonString(householdDto))
                )
                .andExpect(content().json(convertObjectToJsonString(householdDto)))
                .andExpect(jsonPath("$.location").value(updatedLocation))
                .andExpect(status().isOk());

        testLogAppender.assertLoggedLast("PUT: updating household 42");
        verify(householdServiceMock, times(1)).updateHousehold(household);
    }

    @Test
    public void updateHousehold_throws404WhenNotExisting() throws Exception {
        // arrange
        when(householdServiceMock.updateHousehold(any(Household.class))).thenReturn(null);
        household.setId(householdId);

        // act, assert
        mockMvc.perform(put(String.format("/households/%d", householdId))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(convertObjectToJsonString(householdDto))
                )
                .andExpect(jsonPath("$").doesNotExist())
                .andExpect(status().isNotFound())
                .andExpect(result -> {
                    Exception exception = result.getResolvedException();
                    assertTrue(exception instanceof ResourceNotFoundException);
                    assertEquals("Resource of type Household not found with id : '42'", result.getResolvedException().getMessage());
                });

        testLogAppender.assertLogged("PUT: updating household 42");
        verify(householdServiceMock, times(1)).updateHousehold(household);
    }

    @Test
    public void deleteHousehold_returnsNullOnSuccess() throws Exception {
        // arrange
        when(householdServiceMock.deleteHousehold(anyLong())).thenReturn(true);

        // act, assert
        mockMvc.perform(delete(String.format("/households/%d", householdId)))
                .andExpect(jsonPath("$").doesNotExist())
                .andExpect(status().isNoContent());

        testLogAppender.assertLoggedLast("DELETE: deleting household 42");
        verify(householdServiceMock, times(1)).deleteHousehold(householdId);
    }

    @Test
    public void deleteHousehold_returnsPersistedHouseholdOnFailure() throws Exception {
        // arrange
        when(householdServiceMock.deleteHousehold(anyLong())).thenReturn(false);
        when(householdServiceMock.findHouseholdById(anyLong())).thenReturn(household);

        // act, assert
        mockMvc.perform(delete(String.format("/households/%d", householdId)))
                        .andExpect(content().json(convertObjectToJsonString(householdDto)))
                        .andExpect(status().isInternalServerError());

        testLogAppender.assertLogged("DELETE: deleting household 42");
        testLogAppender.assertLoggedLast("household with id 42 could not be deleted!", Level.ERROR);
        verify(householdServiceMock, times(1)).deleteHousehold(householdId);
    }
}
