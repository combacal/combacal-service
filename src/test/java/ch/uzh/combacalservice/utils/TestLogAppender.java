package ch.uzh.combacalservice.utils;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.AppenderBase;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


// SLF4J does not provide built-in features to assert something was properly logged.
// Instead, some workaround is required, see this guide: https://kotlintesting.com/mock-slf4j/
public class TestLogAppender<T> extends AppenderBase<ILoggingEvent> {

    ArrayList<ILoggingEvent> loggingEvents = new ArrayList<>();

    public TestLogAppender(T classEntityUnderTest) {
        super();
        Logger logger = (Logger) LoggerFactory.getLogger(classEntityUnderTest.getClass().getName());
        logger.addAppender(this);
        this.start();
    }

    @Override
    protected void append(ILoggingEvent eventObject) {
        loggingEvents.add(eventObject);
    }

    /***
     * DEPRECATED: use {@link #assertLoggedLast(String)} instead.
     * @return the last recorded log event
     */
    @Deprecated
    public ILoggingEvent getLastLoggedEvent() {
        if (loggingEvents.isEmpty()) {
            return null;
        }
        return loggingEvents.get(loggingEvents.size() - 1);
    }

    public void assertLoggedLast(String logMessage) {
        ILoggingEvent lastLoggedEvent = this.getLastLoggedEvent();
        assertNotNull(lastLoggedEvent);
        assertEquals(logMessage, lastLoggedEvent.getMessage());
    }

    public void assertLoggedLast(String logMessage, Level level) {
        ILoggingEvent lastLoggedEvent = this.getLastLoggedEvent();
        assertNotNull(lastLoggedEvent);
        assertEquals(logMessage, lastLoggedEvent.getMessage());
        assertEquals(level, lastLoggedEvent.getLevel());
    }

    public void assertLogged(String logMessage) {
        ILoggingEvent lastLoggedEvent = this.findLoggedEvent(logMessage);
        assertNotNull(lastLoggedEvent);
        assertEquals(logMessage, lastLoggedEvent.getMessage());
    }

    public void assertLogged(String logMessage, Level level) {
        ILoggingEvent lastLoggedEvent = this.findLoggedEvent(logMessage);
        assertNotNull(lastLoggedEvent);
        assertEquals(logMessage, lastLoggedEvent.getMessage());
        assertEquals(level, lastLoggedEvent.getLevel());
    }

    private ILoggingEvent findLoggedEvent(String logMessage) {
        if (loggingEvents.isEmpty()) {
            return null;
        }
        return loggingEvents.stream().filter(event -> logMessage.equals(event.getMessage())).findFirst().orElse(null);
    }

    private void clearLoggedEvents() {
        loggingEvents.clear();
    }
}
