package ch.uzh.combacalservice.utils;

import ch.uzh.combacalservice.model.medicine.*;
import ch.uzh.combacalservice.model.medicine.dto.*;
import ch.uzh.combacalservice.model.people.Caregiver;
import ch.uzh.combacalservice.model.people.Client;
import ch.uzh.combacalservice.model.people.Community;
import ch.uzh.combacalservice.model.people.Household;
import ch.uzh.combacalservice.model.people.dto.CaregiverDto;
import ch.uzh.combacalservice.model.people.dto.ClientDto;
import ch.uzh.combacalservice.model.people.dto.CommunityDto;
import ch.uzh.combacalservice.model.people.dto.HouseholdDto;
import ch.uzh.combacalservice.model.shared.Constants;
import ch.uzh.combacalservice.model.shared.District;
import ch.uzh.combacalservice.model.shared.Location;
import ch.uzh.combacalservice.model.shared.dto.DistrictDto;
import ch.uzh.combacalservice.model.shared.dto.LocationDto;
import ch.uzh.combacalservice.shared.TestHelpers;
import ch.uzh.combacalservice.util.mapper.MapStructMapper;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;

import static ch.uzh.combacalservice.model.shared.Gender.FEMALE;
import static ch.uzh.combacalservice.model.shared.Gender.MALE;
import static ch.uzh.combacalservice.model.shared.Profession.LAY_HEALTH_WORKER;
import static ch.uzh.combacalservice.shared.TestHelpers.*;
import static org.junit.jupiter.api.Assertions.*;

public class MapStructMapperTest {
    private final MapStructMapper mapper = Mappers.getMapper(MapStructMapper.class);

    //region Mapper tests for model.medicine
    //---------------------------------------------------------------------------------------
    @Test
    public void givenDtoToDomain_of_Disease_whenMaps_thenCorrect() {
        DiseaseDto source = createDiseaseDto();
        Disease destination = mapper.dtoToDomain(source);
        assertEquals(source.getName(), destination.getName());

        // arrange
        SymptomDto symptomDto1 = new SymptomDto();
        symptomDto1.setName("Uncovered Code");
        SymptomDto symptomDto2 = new SymptomDto();
        symptomDto2.setName("Pale Skin");

        TreatmentDto treatmentDto1 = new TreatmentDto();
        treatmentDto1.setName("Write Tests");
        TreatmentDto treatmentDto2 = new TreatmentDto();
        treatmentDto2.setName("Sunbath");

        DiseaseDto diseaseDto = new DiseaseDto();
        diseaseDto.setName("Coding Fever");
        diseaseDto.setSymptoms(Arrays.asList(symptomDto1, symptomDto2));
        diseaseDto.setTreatments(Arrays.asList(treatmentDto1, treatmentDto2));

        // act
        Disease parsed = mapper.dtoToDomain(diseaseDto);

        // assert
        assertNull(parsed.getId());
        assertEquals("Coding Fever", parsed.getName());
        assertNotNull(parsed.getSymptoms());
        assertEquals(2, parsed.getSymptoms().size());
        assertEquals("Uncovered Code", parsed.getSymptoms().get(0).getName());
        assertEquals("Pale Skin", parsed.getSymptoms().get(1).getName());
        assertNotNull(parsed.getTreatments());
        assertEquals(2, parsed.getTreatments().size());
        assertEquals("Write Tests", parsed.getTreatments().get(0).getName());
        assertEquals("Sunbath", parsed.getTreatments().get(1).getName());
    }

    @Test
    public void givenDomainToDto_of_Disease_whenMaps_thenCorrect() {
        Disease source = createDisease();
        DiseaseDto destination = mapper.domainToDto(source);
        assertEquals(source.getName(), destination.getName());

        // arrange
        Symptom symptom1 = new Symptom();
        symptom1.setName("Uncovered Code");
        Symptom symptom2 = new Symptom();
        symptom2.setName("Pale Skin");

        Treatment treatment1 = new Treatment();
        treatment1.setName("Write Tests");
        Treatment treatment2 = new Treatment();
        treatment2.setName("Sunbath");

        Disease disease = new Disease();
        disease.setName("Coding Fever");
        disease.setSymptoms(Arrays.asList(symptom1, symptom2));
        disease.setTreatments(Arrays.asList(treatment1, treatment2));

        // act
        DiseaseDto parsed = mapper.domainToDto(disease);

        // assert
        assertEquals("Coding Fever", parsed.getName());
        assertNotNull(parsed.getSymptoms());
        assertEquals(2, parsed.getSymptoms().size());
        assertEquals("Uncovered Code", parsed.getSymptoms().get(0).getName());
        assertEquals("Pale Skin", parsed.getSymptoms().get(1).getName());
        assertNotNull(parsed.getTreatments());
        assertEquals(2, parsed.getTreatments().size());
        assertEquals("Write Tests", parsed.getTreatments().get(0).getName());
        assertEquals("Sunbath", parsed.getTreatments().get(1).getName());
    }

    @Test
    public void givenDtoToDomain_of_Dossier_whenMaps_thenCorrect() {
        DossierDto source = createDossierDto();
        Dossier destination = mapper.dtoToDomain(source);
        assertEquals(source.getMedicalHistory().size(), destination.getMedicalHistory().size());

        try {
            // arrange
            MedicalHistoryItemDto item1 = new MedicalHistoryItemDto();
            MedicalHistoryItemDto item2 = new MedicalHistoryItemDto();
            Date date1 = new SimpleDateFormat(Constants.DATE_FORMAT).parse("2020-01-01");
            Date date2 = new SimpleDateFormat(Constants.DATE_FORMAT).parse("2020-12-31");
            item1.setTreatmentDate(date1);
            item2.setTreatmentDate(date2);

            DiseaseDto diseaseDto = new DiseaseDto();
            diseaseDto.setName("Coding Fever");
            diseaseDto.setSymptoms(Collections.emptyList());
            diseaseDto.setTreatments(Collections.emptyList());
            item2.setDiseases(Collections.singletonList(diseaseDto));
            item1.setDiseases(Collections.emptyList());

            DossierDto dossierDto = new DossierDto();
            dossierDto.setMedicalHistory(Arrays.asList(item1, item2));

            // act
            Dossier parsed = mapper.dtoToDomain(dossierDto);

            // assert
            assertNull(parsed.getId());
            assertNotNull(parsed.getMedicalHistory());
            assertEquals(2, parsed.getMedicalHistory().size());
            assertEquals(date1, parsed.getMedicalHistory().get(0).getTreatmentDate());
            assertEquals(date2, parsed.getMedicalHistory().get(1).getTreatmentDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void givenDomainToDto_of_Dossier_whenMaps_thenCorrect() {
        Dossier source = createDossier();
        DossierDto destination = mapper.domainToDto(source);
        assertEquals(source.getMedicalHistory().size(), destination.getMedicalHistory().size());

        try {
            // arrange
            MedicalHistoryItem item1 = new MedicalHistoryItem();
            MedicalHistoryItem item2 = new MedicalHistoryItem();
            Date date1 = new SimpleDateFormat(Constants.DATE_FORMAT).parse("2020-01-01");
            Date date2 = new SimpleDateFormat(Constants.DATE_FORMAT).parse("2020-12-31");
            item1.setTreatmentDate(date1);
            item2.setTreatmentDate(date2);

            Disease disease = new Disease();
            disease.setName("Coding Fever");
            disease.setSymptoms(Collections.emptyList());
            disease.setTreatments(Collections.emptyList());
            item2.setDiseases(Collections.singletonList(disease));
            item1.setDiseases(Collections.emptyList());

            Dossier dossier = new Dossier();
            dossier.setMedicalHistory(Arrays.asList(item1, item2));

            // act
            DossierDto parsed = mapper.domainToDto(dossier);

            // assert
            assertNotNull(parsed.getMedicalHistory());
            assertEquals(2, parsed.getMedicalHistory().size());
            assertEquals(date1, parsed.getMedicalHistory().get(0).getTreatmentDate());
            assertEquals(date2, parsed.getMedicalHistory().get(1).getTreatmentDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void givenDtoToDomain_of_MedicalHistoryItem_whenMaps_thenCorrect() {
        MedicalHistoryItemDto source = createMedicalHistoryItemDto();
        MedicalHistoryItem destination = mapper.dtoToDomain(source);
        assertEquals(source.getTreatmentDate(), destination.getTreatmentDate());
        assertEquals(source.getDiseases().size(), destination.getDiseases().size());

        try {
            // arrange
            SymptomDto symptomDto1 = new SymptomDto();
            symptomDto1.setName("Uncovered Code");
            SymptomDto symptomDto2 = new SymptomDto();
            symptomDto2.setName("Pale Skin");

            TreatmentDto treatmentDto1 = new TreatmentDto();
            treatmentDto1.setName("Write Tests");
            TreatmentDto treatmentDto2 = new TreatmentDto();
            treatmentDto2.setName("Sunbath");

            DiseaseDto diseaseDto = new DiseaseDto();
            diseaseDto.setName("Coding Fever");
            diseaseDto.setSymptoms(Arrays.asList(symptomDto1, symptomDto2));
            diseaseDto.setTreatments(Arrays.asList(treatmentDto1, treatmentDto2));

            MedicalHistoryItemDto itemDto = new MedicalHistoryItemDto();
            Date date = new SimpleDateFormat(Constants.DATE_FORMAT).parse("2020-01-01");
            itemDto.setTreatmentDate(date);
            itemDto.setDiseases(Collections.singletonList(diseaseDto));

            // act
            MedicalHistoryItem parsed = mapper.dtoToDomain(itemDto);

            // assert
            assertEquals(date, parsed.getTreatmentDate());
            assertNotNull(parsed.getDiseases());
            assertEquals(1, parsed.getDiseases().size());
            assertEquals("Coding Fever", parsed.getDiseases().get(0).getName());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void givenDomainToDto_of_MedicalHistoryItem_whenMaps_thenCorrect() {
        MedicalHistoryItem source = createMedicalHistoryItem();
        MedicalHistoryItemDto destination = mapper.domainToDto(source);
        assertEquals(source.getTreatmentDate(), destination.getTreatmentDate());
        assertEquals(source.getDiseases().size(), destination.getDiseases().size());

        try {
            // arrange
            Symptom symptom1 = new Symptom();
            symptom1.setName("Uncovered Code");
            Symptom symptom2 = new Symptom();
            symptom2.setName("Pale Skin");

            Treatment treatment1 = new Treatment();
            treatment1.setName("Write Tests");
            Treatment treatment2 = new Treatment();
            treatment2.setName("Sunbath");

            Disease disease = new Disease();
            disease.setName("Coding Fever");
            disease.setSymptoms(Arrays.asList(symptom1, symptom2));
            disease.setTreatments(Arrays.asList(treatment1, treatment2));

            MedicalHistoryItem item = new MedicalHistoryItem();
            Date date = new SimpleDateFormat(Constants.DATE_FORMAT).parse("2020-01-01");
            item.setTreatmentDate(date);
            item.setDiseases(Collections.singletonList(disease));

            // act
            MedicalHistoryItemDto parsed = mapper.domainToDto(item);

            // assert
            assertEquals(date, parsed.getTreatmentDate());
            assertNotNull(parsed.getDiseases());
            assertEquals(1, parsed.getDiseases().size());
            assertEquals("Coding Fever", parsed.getDiseases().get(0).getName());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void givenDtoToDomain_of_Symptom_whenMaps_thenCorrect() {
        SymptomDto source = createSymptomDto();
        Symptom destination = mapper.dtoToDomain(source);
        assertEquals(source.getName(), destination.getName());

        // arrange
        SymptomDto symptomDto = new SymptomDto();
        symptomDto.setName("Testitis");

        // act
        Symptom parsed = mapper.dtoToDomain(symptomDto);

        // assert
        assertNull(parsed.getId());
        assertEquals("Testitis", parsed.getName());
    }

    @Test
    public void givenDomainToDto_of_Symptom_whenMaps_thenCorrect() {
        Symptom source = createSymptom();
        SymptomDto destination = mapper.domainToDto(source);
        assertEquals(source.getName(), destination.getName());

        // arrange
        Symptom symptom = new Symptom();
        symptom.setName("Testitis");

        // act
        SymptomDto parsed = mapper.domainToDto(symptom);

        // assert
        assertEquals("Testitis", parsed.getName());
    }

    @Test
    public void givenDtoToDomain_of_Treatment_whenMaps_thenCorrect() {
        TreatmentDto source = createTreatmentDto();
        Treatment destination = mapper.dtoToDomain(source);
        assertEquals(source.getName(), destination.getName());

        // arrange
        TreatmentDto treatmentDto = new TreatmentDto();
        treatmentDto.setName("Sleep 10h");

        // act
        Treatment parsed = mapper.dtoToDomain(treatmentDto);

        // assert
        assertNull(parsed.getId());
        assertEquals("Sleep 10h", parsed.getName());
    }

    @Test
    public void givenDomainToDto_of_Treatment_whenMaps_thenCorrect() {
        Treatment source = createTreatment();
        TreatmentDto destination = mapper.domainToDto(source);
        assertEquals(source.getName(), destination.getName());

        // arrange
        Treatment treatment = new Treatment();
        treatment.setName("Sleep 10h");

        // act
        TreatmentDto parsed = mapper.domainToDto(treatment);

        // assert
        assertEquals("Sleep 10h", parsed.getName());
    }
    //---------------------------------------------------------------------------------------
    //endregion

    //region Mapper tests for model.people
    //---------------------------------------------------------------------------------------
    @Test
    public void givenDtoToDomain_of_Caregiver_whenMaps_thenCorrect() {
        CaregiverDto source = createCaregiverDto();
        Caregiver destination = mapper.dtoToDomain(source);
        assertEquals(source.getFirstName(), destination.getFirstName());
        assertEquals(source.getLastName(), destination.getLastName());

        // arrange
        CaregiverDto caregiverDto = new CaregiverDto();
        caregiverDto.setFirstName("Eva");
        caregiverDto.setLastName("Carer");

        caregiverDto.setGender(FEMALE);
        caregiverDto.setProfession(LAY_HEALTH_WORKER);

        caregiverDto.setDistrict(createDistrictDto());
        caregiverDto.setCommunities(Collections.singletonList(TestHelpers.createCommunityDto()));

        // act
        Caregiver parsed = mapper.dtoToDomain(caregiverDto);

        // assert
        assertEquals("Eva", parsed.getFirstName());
        assertEquals("Carer", parsed.getLastName());
        assertEquals(FEMALE, parsed.getGender());
        assertEquals(LAY_HEALTH_WORKER, parsed.getProfession());

        assertNotNull(parsed.getDistrict());
        assertEquals("Test District", parsed.getDistrict().getName());

        assertNotNull(parsed.getCommunities());
        assertEquals(1, parsed.getCommunities().size());
        assertNotNull(parsed.getCommunities().get(0).getClients());
        assertEquals(0, parsed.getCommunities().get(0).getClients().size());
    }

    @Test
    public void givenDomainToDto_of_Caregiver_whenMaps_thenCorrect() {
        Caregiver source = createCaregiver();
        CaregiverDto destination = mapper.domainToDto(source);
        assertEquals(source.getFirstName(), destination.getFirstName());
        assertEquals(source.getLastName(), destination.getLastName());

        // arrange
        Caregiver caregiver = new Caregiver();
        caregiver.setFirstName("Eva");
        caregiver.setLastName("Carer");

        caregiver.setGender(FEMALE);
        caregiver.setProfession(LAY_HEALTH_WORKER);

        caregiver.setDistrict(TestHelpers.createDistrict());
        caregiver.setCommunities(Collections.singletonList(TestHelpers.createCommunity()));

        // act
        CaregiverDto parsed = mapper.domainToDto(caregiver);

        // assert
        assertEquals("Eva", parsed.getFirstName());
        assertEquals("Carer", parsed.getLastName());
        assertEquals(FEMALE, parsed.getGender());
        assertEquals(LAY_HEALTH_WORKER, parsed.getProfession());

        assertNotNull(parsed.getDistrict());
        assertEquals("Test District", parsed.getDistrict().getName());

        assertNotNull(parsed.getCommunities());
        assertEquals(1, parsed.getCommunities().size());
        assertNotNull(parsed.getCommunities().get(0).getClients());
        assertEquals(0, parsed.getCommunities().get(0).getClients().size());
    }

    @Test
    public void givenDtoToDomain_of_Client_whenMaps_thenCorrect() {
        ClientDto source = createClientDto();
        Client destination = mapper.dtoToDomain(source);
        assertEquals(source.getFirstName(), destination.getFirstName());
        assertEquals(source.getLastName(), destination.getLastName());

        try {
            // arrange
            ClientDto clientDto = new ClientDto();
            clientDto.setFirstName("Petra");
            clientDto.setLastName("Tester");
            clientDto.setPhoneNumber("079");

            clientDto.setGender(FEMALE);

            Date birthDate = new SimpleDateFormat(Constants.DATE_FORMAT).parse("2020-01-01");
            clientDto.setBirthDate(birthDate);

            ClientDto father = TestHelpers.createClientDto();
            ClientDto mother = TestHelpers.createClientDto();
            mother.setFirstName("Jane");
            clientDto.setParents(Arrays.asList(father, mother));

            clientDto.setHousehold(createHouseholdDto());

            DiseaseDto testitis = createDiseaseDto();
            DiseaseDto codingFever = createDiseaseDto();
            codingFever.setName("Coding Fever");
            clientDto.setDiseases(Arrays.asList(testitis, codingFever));

            clientDto.setMedicalDossier(createDossierDto());

            // act
            Client parsed = mapper.dtoToDomain(clientDto);

            // assert
            assertEquals("Petra", parsed.getFirstName());
            assertEquals("079", parsed.getPhoneNumber());
            assertEquals("Tester", parsed.getLastName());
            assertEquals(FEMALE, parsed.getGender());
            assertEquals(birthDate, parsed.getBirthDate());

            assertNotNull(parsed.getParents());
            assertEquals(2, parsed.getParents().size());
            assertEquals("John", parsed.getParents().get(0).getFirstName());
            assertEquals("Jane", parsed.getParents().get(1).getFirstName());

            assertNotNull(parsed.getHousehold());
            assertEquals("Doe", parsed.getHousehold().getLastName());

            assertNotNull(parsed.getDiseases());
            assertEquals(2, parsed.getDiseases().size());
            assertEquals("Testitis", parsed.getDiseases().get(0).getName());
            assertEquals("Coding Fever", parsed.getDiseases().get(1).getName());

            assertNotNull(parsed.getMedicalDossier());
            assertNotNull(parsed.getMedicalDossier().getMedicalHistory());
            assertEquals(0, parsed.getMedicalDossier().getMedicalHistory().size());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void givenDomainToDto_of_Client_whenMaps_thenCorrect() {
        Client source = createClient();
        ClientDto destination = mapper.domainToDto(source);
        assertEquals(source.getFirstName(), destination.getFirstName());
        assertEquals(source.getLastName(), destination.getLastName());

        try {
            // arrange
            Client client = new Client();
            client.setFirstName("Petra");
            client.setLastName("Tester");
            client.setPhoneNumber("079");

            client.setGender(FEMALE);

            Date birthDate = new SimpleDateFormat(Constants.DATE_FORMAT).parse("2020-01-01");
            client.setBirthDate(birthDate);

            Client father = createClient();
            Client mother = createClient();
            mother.setFirstName("Jane");
            client.setParents(Arrays.asList(father, mother));

            client.setHousehold(createHousehold());

            Disease testitis = createDisease();
            Disease codingFever = createDisease();
            codingFever.setName("Coding Fever");
            client.setDiseases(Arrays.asList(testitis, codingFever));

            client.setMedicalDossier(createDossier());

            // act
            ClientDto parsed = mapper.domainToDto(client);

            // assert
            assertEquals("Petra", parsed.getFirstName());
            assertEquals("079", parsed.getPhoneNumber());
            assertEquals("Tester", parsed.getLastName());
            assertEquals(FEMALE, parsed.getGender());
            assertEquals(birthDate, parsed.getBirthDate());

            assertNotNull(parsed.getParents());
            assertEquals(2, parsed.getParents().size());
            assertEquals("John", parsed.getParents().get(0).getFirstName());
            assertEquals("Jane", parsed.getParents().get(1).getFirstName());

            assertNotNull(parsed.getHousehold());
            assertEquals("Doe", parsed.getHousehold().getLastName());

            assertNotNull(parsed.getDiseases());
            assertEquals(2, parsed.getDiseases().size());
            assertEquals("Testitis", parsed.getDiseases().get(0).getName());
            assertEquals("Coding Fever", parsed.getDiseases().get(1).getName());

            assertNotNull(parsed.getMedicalDossier());
            assertNotNull(parsed.getMedicalDossier().getMedicalHistory());
            assertEquals(0, parsed.getMedicalDossier().getMedicalHistory().size());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void givenDtoToDomain_of_Community_whenMaps_thenCorrect() {
        CommunityDto source = createCommunityDto();
        Community destination = mapper.dtoToDomain(source);
        assertEquals(source.getClients().size(), destination.getClients().size());

        try {
            // arrange
            ClientDto clientDto = new ClientDto();
            clientDto.setFirstName("Peter");
            clientDto.setLastName("Tester");

            clientDto.setGender(MALE);

            Date birthDate = new SimpleDateFormat(Constants.DATE_FORMAT).parse("2020-01-01");
            clientDto.setBirthDate(birthDate);

            ClientDto father = TestHelpers.createClientDto();
            ClientDto mother = TestHelpers.createClientDto();
            mother.setFirstName("Jane");
            clientDto.setParents(Arrays.asList(father, mother));

            clientDto.setHousehold(TestHelpers.createHouseholdDto());

            DiseaseDto testitis = TestHelpers.createDiseaseDto();
            DiseaseDto codingFever = TestHelpers.createDiseaseDto();
            codingFever.setName("Coding Fever");
            clientDto.setDiseases(Arrays.asList(testitis, codingFever));

            clientDto.setMedicalDossier(TestHelpers.createDossierDto());

            CommunityDto communityDto = new CommunityDto();
            communityDto.setClients(Collections.singletonList(clientDto));

            // act
            Community parsed = mapper.dtoToDomain(communityDto);

            // assert
            assertNull(parsed.getId());
            assertNotNull(parsed.getClients());
            assertEquals(1, parsed.getClients().size());
            Client client = parsed.getClients().get(0);
            assertEquals("Tester", client.getLastName());
            assertNotNull(client.getDiseases());
            assertEquals(2, client.getParents().size());
            assertEquals("Jane", client.getParents().get(1).getFirstName());
            assertNotNull(client.getHousehold());
            assertEquals("Doe", client.getHousehold().getLastName());
            assertEquals(2, client.getDiseases().size());
            assertEquals("Coding Fever", client.getDiseases().get(1).getName());
            assertNotNull(client.getMedicalDossier());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void givenDomainToDto_of_Community_whenMaps_thenCorrect() {
        Community source = createCommunity();
        CommunityDto destination = mapper.domainToDto(source);
        assertEquals(source.getClients().size(), destination.getClients().size());

        try {
            // arrange
            Client client = new Client();
            client.setFirstName("Peter");
            client.setLastName("Tester");

            client.setGender(MALE);

            Date birthDate = new SimpleDateFormat(Constants.DATE_FORMAT).parse("2020-01-01");
            client.setBirthDate(birthDate);

            Client father = TestHelpers.createClient();
            Client mother = TestHelpers.createClient();
            mother.setFirstName("Jane");
            client.setParents(Arrays.asList(father, mother));

            client.setHousehold(TestHelpers.createHousehold());

            Disease testitis = TestHelpers.createDisease();
            Disease codingFever = TestHelpers.createDisease();
            codingFever.setName("Coding Fever");
            client.setDiseases(Arrays.asList(testitis, codingFever));

            client.setMedicalDossier(TestHelpers.createDossier());

            Community community = new Community();
            community.setClients(Collections.singletonList(client));

            // act
            CommunityDto parsed = mapper.domainToDto(community);

            // assert
            assertNotNull(parsed.getClients());
            assertEquals(1, parsed.getClients().size());
            ClientDto clientDto = parsed.getClients().get(0);
            assertEquals("Tester", clientDto.getLastName());
            assertNotNull(clientDto.getDiseases());
            assertEquals(2, clientDto.getParents().size());
            assertEquals("Jane", clientDto.getParents().get(1).getFirstName());
            assertNotNull(clientDto.getHousehold());
            assertEquals("Doe", clientDto.getHousehold().getLastName());
            assertEquals(2, clientDto.getDiseases().size());
            assertEquals("Coding Fever", clientDto.getDiseases().get(1).getName());
            assertNotNull(clientDto.getMedicalDossier());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void givenDtoToDomain_of_Household_whenMaps_thenCorrect() {
        HouseholdDto source = createHouseholdDto();
        Household destination = mapper.dtoToDomain(source);
        assertEquals(source.getLastName(), destination.getLastName());
        assertEquals(source.getDistrict().getName(), destination.getDistrict().getName());

        // arrange
        DistrictDto districtDto = new DistrictDto();
        districtDto.setName("Test District");

        LocationDto locationDto = new LocationDto();
        locationDto.setLatitude("123");
        locationDto.setLongitude("456");

        HouseholdDto householdDto = new HouseholdDto();
        householdDto.setLastName("Testers");
        householdDto.setDistrict(districtDto);
        householdDto.setLocation(locationDto);

        // act
        Household parsed = mapper.dtoToDomain(householdDto);

        // assert
        assertNull(parsed.getId());
        assertEquals("Testers", parsed.getLastName());
        assertNotNull(parsed.getDistrict());
        assertEquals("Test District", parsed.getDistrict().getName());
        assertNotNull(parsed.getLocation());
        assertEquals("123", parsed.getLocation().getLatitude());
    }

    @Test
    public void givenDomainToDto_of_Household_whenMaps_thenCorrect() {
        Household source = createHousehold();
        HouseholdDto destination = mapper.domainToDto(source);
        assertEquals(source.getLastName(), destination.getLastName());
        assertEquals(source.getDistrict().getName(), destination.getDistrict().getName());

        // arrange
        District district = new District();
        district.setName("Test District");

        Location location = new Location();
        location.setLatitude("123");
        location.setLongitude("456");

        Household household = new Household();
        household.setLastName("Testers");
        household.setDistrict(district);
        household.setLocation(location);

        // act
        HouseholdDto parsed = mapper.domainToDto(household);

        // assert
        assertEquals("Testers", parsed.getLastName());
        assertNotNull(parsed.getDistrict());
        assertEquals("Test District", parsed.getDistrict().getName());
        assertNotNull(parsed.getLocation());
        assertEquals("123", parsed.getLocation().getLatitude());
    }
    //---------------------------------------------------------------------------------------
    //endregion

    //region Mapper tests for model.shared
    //---------------------------------------------------------------------------------------
    @Test
    public void givenDtoToDomain_of_District_whenMaps_thenCorrect() {
        DistrictDto source = createDistrictDto();
        District destination = mapper.dtoToDomain(source);
        assertEquals(source.getName(), destination.getName());

        // arrange
        DistrictDto districtDto = new DistrictDto();
        districtDto.setName("Test District");

        // act
        District parsed = mapper.dtoToDomain(districtDto);

        // assert
        assertNull(parsed.getId());
        assertEquals("Test District", parsed.getName());
    }

    @Test
    public void givenDomainToDto_of_District_whenMaps_thenCorrect() {
        District source = createDistrict();
        DistrictDto destination = mapper.domainToDto(source);
        assertEquals(source.getName(), destination.getName());

        // arrange
        District district = new District();
        district.setName("Test District");

        // act
        DistrictDto parsed = mapper.domainToDto(district);

        // assert
        assertEquals("Test District", parsed.getName());
    }

    @Test
    public void givenDtoToDomain_of_Location_whenMaps_thenCorrect() {
        LocationDto source = createLocationDto();
        Location destination = mapper.dtoToDomain(source);
        assertEquals(source.getLatitude(), destination.getLatitude());

        // arrange
        LocationDto locationDto = new LocationDto();
        locationDto.setLatitude("123");
        locationDto.setLongitude("456");

        // act
        Location parsed = mapper.dtoToDomain(locationDto);

        // assert
        assertNull(parsed.getId());
        assertEquals("123", parsed.getLatitude());
        assertEquals("456", parsed.getLongitude());
    }

    @Test
    public void givenDomainToDto_of_Location_whenMaps_thenCorrect() {
        Location source = createLocation();
        LocationDto destination = mapper.domainToDto(source);
        assertEquals(source.getLatitude(), destination.getLatitude());

        // arrange
        Location location = new Location();
        location.setLatitude("123");
        location.setLongitude("456");

        // act
        LocationDto parsed = mapper.domainToDto(location);

        // assert
        assertEquals("123", parsed.getLatitude());
        assertEquals("456", parsed.getLongitude());
    }
    //---------------------------------------------------------------------------------------
    //endregion

}
