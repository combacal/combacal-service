package ch.uzh.combacalservice.service.impl;

import ch.uzh.combacalservice.model.medicine.Dossier;
import ch.uzh.combacalservice.repository.IDossierRepository;
import ch.uzh.combacalservice.service.IDossierService;
import ch.uzh.combacalservice.service.IMedicalHistoryItemService;
import ch.uzh.combacalservice.shared.TestHelpers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockedStatic;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class DossierServiceImplTest {
    private DossierServiceImpl service;

    @Mock
    private IDossierRepository dossierRepositoryMock;
    @Mock
    private IMedicalHistoryItemService medicalHistoryItemServiceMock;

    private Dossier dossier;
    private Long dossierId;

    @BeforeEach
    public void setup() {
        dossierRepositoryMock = mock(IDossierRepository.class);
        medicalHistoryItemServiceMock = mock(IMedicalHistoryItemService.class);
        service = new DossierServiceImpl(dossierRepositoryMock, medicalHistoryItemServiceMock);

        dossier = TestHelpers.createDossier();
        dossierId = 42L;
    }

    @Test
    public void findDossierById() {
        // arrange
        when(dossierRepositoryMock.findById(anyLong())).thenReturn(Optional.ofNullable(dossier));

        // act
        Dossier result = service.findDossierById(42L);

        // assert
        verify(dossierRepositoryMock, times(1)).findById(42L);
        assertEquals(dossier, result);
    }

    @Test
    public void createDossier_createsNewDossier() {
        // arrange
        try (MockedStatic<IDossierService> serviceMockedStatic = mockStatic(IDossierService.class)) {

            when(dossierRepositoryMock.save(any(Dossier.class))).thenReturn(dossier);

            // act
            Dossier result = service.createDossier(dossier);

            // assert
            verify(dossierRepositoryMock, times(1)).save(dossier);
            assertEquals(dossier, result);
        }
    }

    @Test
    public void updateDossier_returnsDossierWhenExisting() {
        // arrange
        dossier.setId(dossierId);
        when(dossierRepositoryMock.findById(anyLong())).thenReturn(Optional.ofNullable(dossier));
        when(dossierRepositoryMock.save(any(Dossier.class))).thenReturn(dossier);

        // act
        Dossier result = service.updateDossier(dossier);

        // assert
        verify(dossierRepositoryMock, times(1)).findById(42L);
        verify(dossierRepositoryMock, times(1)).save(dossier);
        assertEquals(dossier, result);
    }

    @Test
    public void updateDossier_returnsNullWhenNotExisting() {
        // arrange
        dossier.setId(dossierId);

        // act
        Dossier result = service.updateDossier(dossier);

        // assert
        verify(dossierRepositoryMock, times(1)).findById(42L);
        verify(dossierRepositoryMock, times(0)).save(dossier);
        assertNull(result);
    }
}
