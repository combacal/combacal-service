package ch.uzh.combacalservice.service.impl;

import ch.uzh.combacalservice.model.medicine.Disease;
import ch.uzh.combacalservice.repository.IDiseaseRepository;
import ch.uzh.combacalservice.service.ISymptomService;
import ch.uzh.combacalservice.service.ITreatmentService;
import ch.uzh.combacalservice.shared.TestHelpers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class DiseaseServiceImplTest {
    private DiseaseServiceImpl service;

    @Mock
    private IDiseaseRepository diseaseRepositoryMock;

    @Mock
    private ISymptomService symptomServiceMock;

    @Mock
    private ITreatmentService treatmentServiceMock;

    private Disease disease;
    private Long diseaseId;

    @BeforeEach
    public void setup() {
        diseaseRepositoryMock = mock(IDiseaseRepository.class);
        symptomServiceMock = mock(ISymptomService.class);
        treatmentServiceMock = mock(ITreatmentService.class);
        service = new DiseaseServiceImpl(diseaseRepositoryMock, symptomServiceMock, treatmentServiceMock);

        disease = TestHelpers.createDisease();
        diseaseId = 42L;
    }

    @Test
    public void findDiseaseById() {
        // arrange
        when(diseaseRepositoryMock.findById(anyLong())).thenReturn(Optional.ofNullable(disease));

        // act
        Disease result = service.findDiseaseById(42L);

        // assert
        verify(diseaseRepositoryMock, times(1)).findById(42L);
        assertEquals(disease, result);
    }

    @Test
    public void createDisease_createsNewDisease() {
        // arrange
        when(diseaseRepositoryMock.save(any(Disease.class))).thenReturn(disease);

        // act
        Disease result = service.createDisease(disease);

        // assert
        verify(diseaseRepositoryMock, times(1)).save(disease);
        assertEquals(disease, result);
    }

    @Test
    public void updateDisease_returnsDiseaseWhenExisting() {
        // arrange
        disease.setId(diseaseId);
        when(diseaseRepositoryMock.findById(anyLong())).thenReturn(Optional.ofNullable(disease));
        when(diseaseRepositoryMock.save(any(Disease.class))).thenReturn(disease);

        // act
        Disease result = service.updateDisease(disease);

        // assert
        verify(diseaseRepositoryMock, times(1)).findById(42L);
        verify(diseaseRepositoryMock, times(1)).save(disease);
        assertEquals(disease, result);
    }

    @Test
    public void updateDisease_returnsNullWhenNotExisting() {
        // arrange
        disease.setId(diseaseId);

        // act
        Disease result = service.updateDisease(disease);

        // assert
        verify(diseaseRepositoryMock, times(1)).findById(42L);
        verify(diseaseRepositoryMock, times(0)).save(disease);
        assertNull(result);
    }
}
