package ch.uzh.combacalservice.service.impl;

import ch.uzh.combacalservice.model.admin.AuthGroups;
import ch.uzh.combacalservice.model.admin.User;
import ch.uzh.combacalservice.model.admin.UserHistory;
import ch.uzh.combacalservice.model.admin.dto.UserDto;
import ch.uzh.combacalservice.repository.UserHistoryRepository;
import ch.uzh.combacalservice.repository.UserRepository;
import ch.uzh.combacalservice.security.AuthenticationRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest // requires running database instances for test execution
class AdminServiceTest {

    private AdminService adminService;

    @Mock
    UserRepository userRepository;
    @Mock
    UserHistoryRepository userHistoryRepository;

    @BeforeEach
    void setup() {
        this.adminService = new AdminService(new AccessService(userRepository, userHistoryRepository));
        User user = new User();
        user.setUsername("Exists");
        user.setPassword("anything");
        user.setAuthGroups(new ArrayList<>(List.of(AuthGroups.USER)));
        User admin = new User();
        admin.setUsername("Principal");
        admin.setPassword("anything");
        admin.setAuthGroups(new ArrayList<>(List.of(AuthGroups.ADMIN)));
        when(userRepository.findByUsername("NotExists")).thenReturn(Optional.empty());
        when(userRepository.findByUsername("Exists")).thenReturn(Optional.of(user));
        when(userRepository.findByUsername("Principal")).thenReturn(Optional.of(admin));
        when(userRepository.save(Mockito.any(User.class)))
                .thenAnswer(i -> i.getArguments()[0]);
        when(userHistoryRepository.save(Mockito.any(UserHistory.class)))
                .thenAnswer(i -> i.getArguments()[0]);
    }

    @Test
    void createUser() {
        // arrange
        AuthenticationRequest request = AuthenticationRequest.builder().username("test").password("pw").build();

        // act
        UserDto user = adminService.createUser(request, "Principal");

        // assert
        assertNotNull(user.getAuthGroups(), "authgroups should be set");
        assertEquals(1, user.getAuthGroups().size(), "authgroups should contain one element");
        assertEquals(AuthGroups.USER, user.getAuthGroups().get(0), "authgroups should contain USER");
        assertTrue(user.getEnabled(), "User should be enabled");
        verify(userHistoryRepository, times(1)).save(any(UserHistory.class));
    }

    @Test
    void createAdmin() {
        // arrange
        AuthenticationRequest request = AuthenticationRequest.builder().username("test").password("pw").build();

        // act
        UserDto user = adminService.createAdmin(request, "Principal");

        // assert
        assertNotNull(user.getAuthGroups(), "authgroups should be set");
        assertEquals(1, user.getAuthGroups().size(), "authgroups should contain one element");
        assertEquals(AuthGroups.ADMIN, user.getAuthGroups().get(0), "authgroups should contain ADMIN");
        assertTrue(user.getEnabled(), "User should be enabled");
        verify(userHistoryRepository, times(1)).save(any(UserHistory.class));
    }
}
