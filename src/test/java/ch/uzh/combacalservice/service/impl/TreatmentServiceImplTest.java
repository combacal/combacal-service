package ch.uzh.combacalservice.service.impl;

import ch.uzh.combacalservice.model.medicine.Treatment;
import ch.uzh.combacalservice.repository.ITreatmentRepository;
import ch.uzh.combacalservice.shared.TestHelpers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class TreatmentServiceImplTest {
    private TreatmentServiceImpl service;

    @Mock
    private ITreatmentRepository treatmentRepositoryMock;
    private Treatment treatment;
    private Long treatmentId;

    @BeforeEach
    public void setup() {
        treatmentRepositoryMock = mock(ITreatmentRepository.class);
        service = new TreatmentServiceImpl(treatmentRepositoryMock);

        treatment = TestHelpers.createTreatment();
        treatmentId = 42L;
    }

    @Test
    public void findTreatmentById() {
        // arrange
        when(treatmentRepositoryMock.findById(anyLong())).thenReturn(Optional.ofNullable(treatment));

        // act
        Treatment result = service.findTreatmentById(42L);

        // assert
        verify(treatmentRepositoryMock, times(1)).findById(42L);
        assertEquals(treatment, result);
    }

    @Test
    public void createTreatment_createsNewTreatment() {
        // arrange
        when(treatmentRepositoryMock.save(any(Treatment.class))).thenReturn(treatment);

        // act
        Treatment result = service.createTreatment(treatment);

        // assert
        verify(treatmentRepositoryMock, times(1)).save(treatment);
        assertEquals(treatment, result);
    }

    @Test
    public void updateTreatment_returnsTreatmentWhenExisting() {
        // arrange
        treatment.setId(treatmentId);
        when(treatmentRepositoryMock.findById(anyLong())).thenReturn(Optional.ofNullable(treatment));
        when(treatmentRepositoryMock.save(any(Treatment.class))).thenReturn(treatment);

        // act
        Treatment result = service.updateTreatment(treatment);

        // assert
        verify(treatmentRepositoryMock, times(1)).findById(42L);
        verify(treatmentRepositoryMock, times(1)).save(treatment);
        assertEquals(treatment, result);
    }

    @Test
    public void updateTreatment_returnsNullWhenNotExisting() {
        // arrange
        treatment.setId(treatmentId);

        // act
        Treatment result = service.updateTreatment(treatment);

        // assert
        verify(treatmentRepositoryMock, times(1)).findById(42L);
        verify(treatmentRepositoryMock, times(0)).save(treatment);
        assertNull(result);
    }
}
