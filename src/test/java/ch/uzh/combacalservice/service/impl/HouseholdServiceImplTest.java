package ch.uzh.combacalservice.service.impl;

import ch.uzh.combacalservice.model.people.Household;
import ch.uzh.combacalservice.model.shared.Location;
import ch.uzh.combacalservice.repository.IHouseholdRepository;
import ch.uzh.combacalservice.shared.TestHelpers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class HouseholdServiceImplTest {
    private HouseholdServiceImpl service;

    @Mock
    private IHouseholdRepository householdRepositoryMock;
    @Mock
    private DistrictServiceImpl districtServiceMock;
    @Mock
    private LocationServiceImpl locationServiceMock;

    private Household household;
    private Long householdId;

    @BeforeEach
    public void setup() {
        householdRepositoryMock = mock(IHouseholdRepository.class);
        districtServiceMock = mock(DistrictServiceImpl.class);
        locationServiceMock = mock(LocationServiceImpl.class);
        service = new HouseholdServiceImpl(householdRepositoryMock, districtServiceMock, locationServiceMock);

        household = TestHelpers.createHousehold();
        householdId = 42L;
    }

    @Test
    public void getAllHouseholds() {
        // arrange
        List<Household> households = Arrays.asList(household, TestHelpers.createHousehold());
        when(householdRepositoryMock.findAll()).thenReturn(households);

        // act
        Iterable<Household> result = service.getAllHouseholds();

        // assert
        verify(householdRepositoryMock, times(1)).findAll();
        assertEquals(households, result);
    }

    @Test
    public void findHouseholdById() {
        // arrange
        when(householdRepositoryMock.findById(anyLong())).thenReturn(Optional.ofNullable(household));

        // act
        Household result = service.findHouseholdById(42L);

        // assert
        verify(householdRepositoryMock, times(1)).findById(42L);
        assertEquals(household, result);
    }

    @Test
    public void findHouseholdsByLastName() {
        // arrange
        List<Household> households = Collections.singletonList(household);
        when(householdRepositoryMock.findByLastName(anyString())).thenReturn(households);

        // act
        List<Household> result = service.findHouseholdsByLastName("Doe");

        // assert
        verify(householdRepositoryMock, times(1)).findByLastName("Doe");
        assertEquals(households, result);
    }

    @Test
    public void createHousehold_createsNewHousehold() {
        // arrange
        when(householdRepositoryMock.save(any(Household.class))).thenReturn(household);

        // act
        Household result = service.createHousehold(household);

        // assert
        verify(householdRepositoryMock, times(1)).save(household);
        assertEquals(household, result);
    }

    @Test
    public void updateHousehold_returnsHouseholdWhenExisting() {
        // arrange
        household.setId(householdId);
        Location updatedLocation = new Location();
        updatedLocation.setLatitude("123.45");
        updatedLocation.setLongitude("678.09");
        household.setLocation(updatedLocation);
        when(householdRepositoryMock.findById(anyLong())).thenReturn(Optional.ofNullable(household));
        when(householdRepositoryMock.save(any(Household.class))).thenReturn(household);

        // act
        Household result = service.updateHousehold(household);

        // assert
        verify(householdRepositoryMock, times(1)).findById(42L);
        verify(householdRepositoryMock, times(1)).save(household);
        assertEquals(household, result);
        assertEquals(updatedLocation, result.getLocation());
    }

    @Test
    public void updateHousehold_returnsNullWhenNotExisting() {
        // arrange
        household.setId(householdId);

        // act
        Household result = service.updateHousehold(household);

        // assert
        verify(householdRepositoryMock, times(1)).findById(42L);
        verify(householdRepositoryMock, times(0)).save(household);
        assertNull(result);
    }

    @Test
    public void deleteHousehold_returnsTrueWhenNotExisting() {
        // arrange
        when(householdRepositoryMock.findById(anyLong())).thenReturn(Optional.empty());

        // act
        boolean result = service.deleteHousehold(42L);

        // assert
        verify(householdRepositoryMock, times(1)).findById(42L);
        verify(householdRepositoryMock, times(0)).delete(household);
        verify(householdRepositoryMock, times(0)).existsById(42L);
        assertTrue(result);
    }

    @Test
    public void deleteHousehold_returnsTrueOnSuccess() {
        // arrange
        when(householdRepositoryMock.findById(anyLong())).thenReturn(Optional.ofNullable(household));

        // act
        boolean result = service.deleteHousehold(42L);

        // assert
        verify(householdRepositoryMock, times(1)).findById(42L);
        verify(householdRepositoryMock, times(1)).delete(household);
        verify(householdRepositoryMock, times(1)).existsById(42L);
        assertTrue(result);
    }

    @Test
    public void deleteHousehold_returnsFalseOnFailure() {
        // arrange
        when(householdRepositoryMock.findById(anyLong())).thenReturn(Optional.ofNullable(household));
        when(householdRepositoryMock.existsById(anyLong())).thenReturn(true);

        // act
        boolean result = service.deleteHousehold(42L);

        // assert
        verify(householdRepositoryMock, times(1)).findById(42L);
        verify(householdRepositoryMock, times(1)).delete(household);
        verify(householdRepositoryMock, times(1)).existsById(42L);
        assertFalse(result);
    }
}
