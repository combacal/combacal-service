package ch.uzh.combacalservice.service.impl;

import ch.uzh.combacalservice.model.shared.District;
import ch.uzh.combacalservice.repository.IDistrictRepository;
import ch.uzh.combacalservice.shared.TestHelpers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class DistrictServiceImplTest {
    private DistrictServiceImpl service;

    @Mock
    private IDistrictRepository districtRepositoryMock;

    private District district;
    private Long districtId;

    @BeforeEach
    public void setup() {
        districtRepositoryMock = mock(IDistrictRepository.class);
        service = new DistrictServiceImpl(districtRepositoryMock);

        district = TestHelpers.createDistrict();
        districtId = 42L;
    }

    @Test
    public void findDistrictById() {
        // arrange
        when(districtRepositoryMock.findById(anyLong())).thenReturn(Optional.ofNullable(district));

        // act
        District result = service.findDistrictById(42L);

        // assert
        verify(districtRepositoryMock, times(1)).findById(42L);
        assertEquals(district, result);
    }

    @Test
    public void createDistrict_createsNewDistrict() {
        // arrange
        when(districtRepositoryMock.save(any(District.class))).thenReturn(district);

        // act
        District result = service.createDistrict(district);

        // assert
        verify(districtRepositoryMock, times(1)).save(district);
        assertEquals(district, result);
    }

    @Test
    public void updateDistrict_returnsDistrictWhenExisting() {
        // arrange
        district.setId(districtId);
        when(districtRepositoryMock.findById(anyLong())).thenReturn(Optional.ofNullable(district));
        when(districtRepositoryMock.save(any(District.class))).thenReturn(district);

        // act
        District result = service.updateDistrict(district);

        // assert
        verify(districtRepositoryMock, times(1)).findById(42L);
        verify(districtRepositoryMock, times(1)).save(district);
        assertEquals(district, result);
    }

    @Test
    public void updateDistrict_returnsNullWhenNotExisting() {
        // arrange
        district.setId(districtId);

        // act
        District result = service.updateDistrict(district);

        // assert
        verify(districtRepositoryMock, times(1)).findById(42L);
        verify(districtRepositoryMock, times(0)).save(district);
        assertNull(result);
    }
}
