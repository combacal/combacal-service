package ch.uzh.combacalservice.service.impl;

import ch.uzh.combacalservice.model.people.Caregiver;
import ch.uzh.combacalservice.model.people.Community;
import ch.uzh.combacalservice.model.shared.District;
import ch.uzh.combacalservice.repository.ICaregiverRepository;
import ch.uzh.combacalservice.service.ICommunityService;
import ch.uzh.combacalservice.service.IDistrictService;
import ch.uzh.combacalservice.shared.TestHelpers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class CaregiverServiceImplTest {
    private CaregiverServiceImpl service;

    @Mock
    private ICaregiverRepository caregiverRepositoryMock;
    @Mock
    private IDistrictService districtServiceMock;
    @Mock
    private ICommunityService communityServiceMock;

    private Caregiver caregiver;
    private Long caregiverId;

    @BeforeEach
    public void setup() {
        caregiverRepositoryMock = mock(ICaregiverRepository.class);
        districtServiceMock = mock(IDistrictService.class);
        communityServiceMock = mock(ICommunityService.class);
        service = spy(new CaregiverServiceImpl(caregiverRepositoryMock, districtServiceMock, communityServiceMock));

        caregiver = TestHelpers.createCaregiver();
        caregiverId = 42L;
    }

    @Test
    public void getAllCaregivers() {
        // arrange
        List<Caregiver> caregivers = Arrays.asList(caregiver, TestHelpers.createCaregiver());
        when(caregiverRepositoryMock.findAll()).thenReturn(caregivers);

        // act
        Iterable<Caregiver> result = service.getAllCaregivers();

        // assert
        verify(caregiverRepositoryMock, times(1)).findAll();
        assertEquals(caregivers, result);
    }

    @Test
    public void findCaregiverById() {
        // arrange
        when(caregiverRepositoryMock.findById(anyLong())).thenReturn(Optional.ofNullable(caregiver));

        // act
        Caregiver result = service.findCaregiverById(42L);

        // assert
        verify(caregiverRepositoryMock, times(1)).findById(42L);
        assertEquals(caregiver, result);
    }

    @Test
    public void createCaregiver_createsNewCaregiver() {
        // arrange
        caregiver.setId(caregiverId);
        when(caregiverRepositoryMock.save(any(Caregiver.class))).thenReturn(caregiver);

        // act
        Caregiver result = service.createCaregiver(caregiver);

        // assert
        verify(districtServiceMock, times(1)).createDistrict(any(District.class));
        verify(communityServiceMock, times(0)).createCommunity(any(Community.class));
        verify(service, times(1)).createCaregiver(any(Caregiver.class));
        verify(caregiverRepositoryMock, times(1)).save(caregiver);
        assertEquals(caregiver, result);
    }

    @Test
    public void updateCaregiver_returnsCaregiverWhenExisting() {
        // arrange
        caregiver.setId(caregiverId);
        when(caregiverRepositoryMock.findById(anyLong())).thenReturn(Optional.ofNullable(caregiver));
        when(caregiverRepositoryMock.save(any(Caregiver.class))).thenReturn(caregiver);
        // TODO: test for actual updates

        // act
        Caregiver result = service.updateCaregiver(caregiver);

        // assert
        verify(caregiverRepositoryMock, times(1)).findById(42L);
        verify(caregiverRepositoryMock, times(1)).save(caregiver);
        assertEquals(caregiver, result);
    }

    @Test
    public void updateCaregiver_returnsNullWhenNotExisting() {
        // arrange
        caregiver.setId(caregiverId);

        // act
        Caregiver result = service.updateCaregiver(caregiver);

        // assert
        verify(caregiverRepositoryMock, times(1)).findById(42L);
        verify(caregiverRepositoryMock, times(0)).save(caregiver);
        assertNull(result);
    }
}
