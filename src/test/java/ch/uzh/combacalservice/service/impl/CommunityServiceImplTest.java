package ch.uzh.combacalservice.service.impl;

import ch.uzh.combacalservice.model.people.Community;
import ch.uzh.combacalservice.repository.ICommunityRepository;
import ch.uzh.combacalservice.service.IClientService;
import ch.uzh.combacalservice.shared.TestHelpers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class CommunityServiceImplTest {
    private CommunityServiceImpl service;

    @Mock
    private ICommunityRepository communityRepositoryMock;
    @Mock
    private IClientService clientServiceMock;

    private Community community;
    private Long communityId;

    @BeforeEach
    public void setup() {
        communityRepositoryMock = mock(ICommunityRepository.class);
        clientServiceMock = mock(IClientService.class);
        service = new CommunityServiceImpl(communityRepositoryMock, clientServiceMock);

        community = TestHelpers.createCommunity();
        communityId = 42L;
    }

    @Test
    public void findCommunityById() {
        // arrange
        when(communityRepositoryMock.findById(anyLong())).thenReturn(Optional.ofNullable(community));

        // act
        Community result = service.findCommunityById(42L);

        // assert
        verify(communityRepositoryMock, times(1)).findById(42L);
        assertEquals(community, result);
    }

    @Test
    public void createCommunity_createsNewCommunity() {
        // arrange
        when(communityRepositoryMock.save(any(Community.class))).thenReturn(community);

        // act
        Community result = service.createCommunity(community);

        // assert
        verify(communityRepositoryMock, times(1)).save(community);
        assertEquals(community, result);
    }

    @Test
    public void updateCommunity_returnsCommunityWhenExisting() {
        // arrange
        community.setId(communityId);
        when(communityRepositoryMock.findById(anyLong())).thenReturn(Optional.ofNullable(community));
        when(communityRepositoryMock.save(any(Community.class))).thenReturn(community);

        // act
        Community result = service.updateCommunity(community);

        // assert
        verify(communityRepositoryMock, times(1)).findById(42L);
        verify(communityRepositoryMock, times(1)).save(community);
        assertEquals(community, result);
    }

    @Test
    public void updateCommunity_returnsNullWhenNotExisting() {
        // arrange
        community.setId(communityId);

        // act
        Community result = service.updateCommunity(community);

        // assert
        verify(communityRepositoryMock, times(1)).findById(42L);
        verify(communityRepositoryMock, times(0)).save(community);
        assertNull(result);
    }
}
