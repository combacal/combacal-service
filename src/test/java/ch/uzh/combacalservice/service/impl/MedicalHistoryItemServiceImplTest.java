package ch.uzh.combacalservice.service.impl;

import ch.uzh.combacalservice.model.medicine.MedicalHistoryItem;
import ch.uzh.combacalservice.repository.IMedicalHistoryItemRepository;
import ch.uzh.combacalservice.service.IDiseaseService;
import ch.uzh.combacalservice.shared.TestHelpers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class MedicalHistoryItemServiceImplTest {
    private MedicalHistoryItemServiceImpl service;

    @Mock
    private IMedicalHistoryItemRepository medicalHistoryItemRepository;
    @Mock
    private IDiseaseService diseaseServiceMock;

    private MedicalHistoryItem medicalHistoryItem;
    private Long medicalHistoryItemId;

    @BeforeEach
    public void setup() {
        medicalHistoryItemRepository = mock(IMedicalHistoryItemRepository.class);
        diseaseServiceMock = mock(IDiseaseService.class);
        service = new MedicalHistoryItemServiceImpl(medicalHistoryItemRepository, diseaseServiceMock);

        medicalHistoryItem = TestHelpers.createMedicalHistoryItem();
        medicalHistoryItemId = 42L;
    }

    @Test
    public void findMedicalHistoryItemById() {
        // arrange
        when(medicalHistoryItemRepository.findById(anyLong())).thenReturn(Optional.ofNullable(medicalHistoryItem));

        // act
        MedicalHistoryItem result = service.findMedicalHistoryItemById(42L);

        // assert
        verify(medicalHistoryItemRepository, times(1)).findById(42L);
        assertEquals(medicalHistoryItem, result);
    }

    @Test
    public void createMedicalHistoryItem_createsNewMedicalHistoryItem() {
        // arrange
        when(medicalHistoryItemRepository.save(any(MedicalHistoryItem.class))).thenReturn(medicalHistoryItem);

        // act
        MedicalHistoryItem result = service.createMedicalHistoryItem(medicalHistoryItem);

        // assert
        verify(medicalHistoryItemRepository, times(1)).save(medicalHistoryItem);
        assertEquals(medicalHistoryItem, result);
    }

    @Test
    public void updateMedicalHistoryItem_returnsItemWhenExisting() {
        // arrange
        medicalHistoryItem.setId(medicalHistoryItemId);
        when(medicalHistoryItemRepository.findById(anyLong())).thenReturn(Optional.ofNullable(medicalHistoryItem));
        when(medicalHistoryItemRepository.save(any(MedicalHistoryItem.class))).thenReturn(medicalHistoryItem);

        // act
        MedicalHistoryItem result = service.updateMedicalHistoryItem(medicalHistoryItem);

        // assert
        verify(medicalHistoryItemRepository, times(1)).findById(42L);
        verify(medicalHistoryItemRepository, times(1)).save(medicalHistoryItem);
        assertEquals(medicalHistoryItem, result);
    }

    @Test
    public void updateMedicalHistoryItem_returnsNullWhenNotExisting() {
        // arrange
        medicalHistoryItem.setId(medicalHistoryItemId);

        // act
        MedicalHistoryItem result = service.updateMedicalHistoryItem(medicalHistoryItem);

        // assert
        verify(medicalHistoryItemRepository, times(1)).findById(42L);
        verify(medicalHistoryItemRepository, times(0)).save(medicalHistoryItem);
        assertNull(result);
    }
}
