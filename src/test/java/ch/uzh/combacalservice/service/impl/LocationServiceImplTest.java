package ch.uzh.combacalservice.service.impl;

import ch.uzh.combacalservice.model.shared.Location;
import ch.uzh.combacalservice.repository.ILocationRepository;
import ch.uzh.combacalservice.shared.TestHelpers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class LocationServiceImplTest {
    private LocationServiceImpl service;

    @Mock
    private ILocationRepository locationRepositoryMock;

    private Location location;
    private Long locationId;

    @BeforeEach
    public void setup() {
        locationRepositoryMock = mock(ILocationRepository.class);
        service = new LocationServiceImpl(locationRepositoryMock);

        location = TestHelpers.createLocation();
        locationId = 42L;
    }

    @Test
    public void findLocationById() {
        // arrange
        when(locationRepositoryMock.findById(anyLong())).thenReturn(Optional.ofNullable(location));

        // act
        Location result = service.findLocationById(42L);

        // assert
        verify(locationRepositoryMock, times(1)).findById(42L);
        assertEquals(location, result);
    }

    @Test
    public void createLocation_createsNewLocation() {
        // arrange
        when(locationRepositoryMock.save(any(Location.class))).thenReturn(location);

        // act
        Location result = service.createLocation(location);

        // assert
        verify(locationRepositoryMock, times(1)).save(location);
        assertEquals(location, result);
    }

    @Test
    public void updateLocation_returnsLocationWhenExisting() {
        // arrange
        location.setId(locationId);
        when(locationRepositoryMock.findById(anyLong())).thenReturn(Optional.ofNullable(location));
        when(locationRepositoryMock.save(any(Location.class))).thenReturn(location);

        // act
        Location result = service.updateLocation(location);

        // assert
        verify(locationRepositoryMock, times(1)).findById(42L);
        verify(locationRepositoryMock, times(1)).save(location);
        assertEquals(location, result);
    }

    @Test
    public void updateLocation_returnsNullWhenNotExisting() {
        // arrange
        location.setId(locationId);

        // act
        Location result = service.updateLocation(location);

        // assert
        verify(locationRepositoryMock, times(1)).findById(42L);
        verify(locationRepositoryMock, times(0)).save(location);
        assertNull(result);
    }
}
