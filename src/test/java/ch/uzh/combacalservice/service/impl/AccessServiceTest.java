package ch.uzh.combacalservice.service.impl;

import ch.uzh.combacalservice.exception.CustomAuthenticationException;
import ch.uzh.combacalservice.model.admin.AuthGroups;
import ch.uzh.combacalservice.model.admin.User;
import ch.uzh.combacalservice.model.admin.UserHistory;
import ch.uzh.combacalservice.repository.UserHistoryRepository;
import ch.uzh.combacalservice.repository.UserRepository;
import ch.uzh.combacalservice.service.IAccessService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest // requires running database instances for test execution
class AccessServiceTest {

    private IAccessService accessService;

    @Mock
    UserRepository userRepository;
    @Mock
    UserHistoryRepository userHistoryRepository;

    @BeforeEach
    void setup() {
        this.accessService = new AccessService(userRepository, userHistoryRepository);
        User user = new User();
        user.setId(1L);
        user.setUsername("Exists");
        user.setPassword("anything");
        user.setAuthGroups(new ArrayList<>(List.of(AuthGroups.USER)));
        User admin = new User();
        admin.setId(2L);
        admin.setUsername("Principal");
        admin.setPassword("anything");
        admin.setAuthGroups(new ArrayList<>(List.of(AuthGroups.ADMIN)));
        when(userRepository.findByUsername("NotExists")).thenReturn(Optional.empty());
        when(userRepository.findByUsername("Exists")).thenReturn(Optional.of(user));
        when(userRepository.findByUsername("Principal")).thenReturn(Optional.of(admin));
        when(userRepository.findById(1L)).thenReturn(Optional.of(user));
        when(userRepository.save(any(User.class)))
                .thenAnswer(i -> i.getArguments()[0]);
        when(userHistoryRepository.save(any(UserHistory.class)))
                .thenAnswer(i -> i.getArguments()[0]);
    }

    @Test
    void getUserNotExists() {
        assertThrows(CustomAuthenticationException.class, () -> accessService.getUserByUsername("NotExists"),
                "Method call should throw error because user does not exist");
    }

    @Test
    void getUserExists() {
        // act
        User user = accessService.getUserByUsername("Exists");

        // assert
        assertEquals("Exists", user.getUsername(), "Username does not match");
    }

    @Test
    void addUserEmpty() {
        // arrange
        User user = new User();

        // act & assert
        Exception exception = assertThrows(CustomAuthenticationException.class, () -> accessService.addUser(user, "Principal"),
                "Method call should throw error because user is empty");
        assertEquals("Username and password cannot be null", exception.getMessage());
    }

    @Test
    void addUserNotComplete() {
        // arrange
        User user = new User();
        user.setUsername("NewUser");

        // act & assert
        Exception exception = assertThrows(CustomAuthenticationException.class, () -> accessService.addUser(user, "Principal"),
                "Method call should throw error because user is empty");
        assertEquals("Username and password cannot be null", exception.getMessage());
    }

    @Test
    void addUserAlreadyExists() {
        // arrange
        User user = new User();
        user.setUsername("Exists");
        user.setPassword("anything");

        // act & assert
        Exception exception = assertThrows(CustomAuthenticationException.class, () -> accessService.addUser(user, "Principal"),
                "Method call should throw error because user is empty");
        assertEquals("User already exists", exception.getMessage());
    }

    @Test
    void addUser() {
        // act
        User user = new User();
        user.setUsername("NewUser");
        user.setPassword("anything");
        user = accessService.addUser(user, "Principal");

        // assert
        assertNotNull(user);
        assertEquals("NewUser", user.getUsername(), "username does not match");
        assertEquals("anything", user.getPassword(), "password does not match");
        verify(userHistoryRepository, times(1)).save(any(UserHistory.class));
    }

    @Test
    void updateUser() {
        // arrange
        User user = new User();
        user.setId(1L);
        user.setUsername("Exists");
        user.setEnabled(false);

        // act
        User result = accessService.updateUser(user, "Principal");

        // assert
        assertFalse(result.getEnabled());
        assertNotNull(result.getAuthGroups());
    }
}
