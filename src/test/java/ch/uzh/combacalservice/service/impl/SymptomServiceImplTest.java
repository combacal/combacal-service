package ch.uzh.combacalservice.service.impl;

import ch.uzh.combacalservice.model.medicine.Symptom;
import ch.uzh.combacalservice.repository.ISymptomRepository;
import ch.uzh.combacalservice.shared.TestHelpers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class SymptomServiceImplTest {
    private SymptomServiceImpl service;

    @Mock
    private ISymptomRepository symptomRepositoryMock;

    private Symptom symptom;
    private Long symptomId;

    @BeforeEach
    public void setup() {
        symptomRepositoryMock = mock(ISymptomRepository.class);
        service = new SymptomServiceImpl(symptomRepositoryMock);

        symptom = TestHelpers.createSymptom();
        symptomId = 42L;
    }

    @Test
    public void findSymptomById() {
        // arrange
        when(symptomRepositoryMock.findById(anyLong())).thenReturn(Optional.ofNullable(symptom));

        // act
        Symptom result = service.findSymptomById(42L);

        // assert
        verify(symptomRepositoryMock, times(1)).findById(42L);
        assertEquals(symptom, result);
    }

    @Test
    public void createSymptom_createsNewSymptom() {
        // arrange
        when(symptomRepositoryMock.save(any(Symptom.class))).thenReturn(symptom);

        // act
        Symptom result = service.createSymptom(symptom);

        // assert
        verify(symptomRepositoryMock, times(1)).save(symptom);
        assertEquals(symptom, result);
    }

    @Test
    public void updateSymptom_returnsSymptomWhenExisting() {
        // arrange
        symptom.setId(symptomId);
        when(symptomRepositoryMock.findById(anyLong())).thenReturn(Optional.ofNullable(symptom));
        when(symptomRepositoryMock.save(any(Symptom.class))).thenReturn(symptom);

        // act
        Symptom result = service.updateSymptom(symptom);

        // assert
        verify(symptomRepositoryMock, times(1)).findById(42L);
        verify(symptomRepositoryMock, times(1)).save(symptom);
        assertEquals(symptom, result);
    }

    @Test
    public void updateSymptom_returnsNullWhenNotExisting() {
        // arrange
        symptom.setId(symptomId);

        // act
        Symptom result = service.updateSymptom(symptom);

        // assert
        verify(symptomRepositoryMock, times(1)).findById(42L);
        verify(symptomRepositoryMock, times(0)).save(symptom);
        assertNull(result);
    }
}
