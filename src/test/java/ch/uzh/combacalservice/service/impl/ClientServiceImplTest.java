package ch.uzh.combacalservice.service.impl;

import ch.uzh.combacalservice.model.medicine.Disease;
import ch.uzh.combacalservice.model.medicine.Dossier;
import ch.uzh.combacalservice.model.people.Client;
import ch.uzh.combacalservice.repository.IClientRepository;
import ch.uzh.combacalservice.service.IDiseaseService;
import ch.uzh.combacalservice.service.IDossierService;
import ch.uzh.combacalservice.service.IHouseholdService;
import ch.uzh.combacalservice.shared.TestHelpers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class ClientServiceImplTest {
    private ClientServiceImpl serviceSpy;

    @Mock
    private IClientRepository clientRepositoryMock;
    @Mock
    private IHouseholdService householdServiceMock;
    @Mock
    private IDiseaseService diseaseServiceMock;
    @Mock
    private IDossierService dossierServiceMock;

    private Client client;
    private Long clientId;

    @BeforeEach
    public void setup() {
        clientRepositoryMock = mock(IClientRepository.class);
        householdServiceMock = mock(IHouseholdService.class);
        diseaseServiceMock = mock(IDiseaseService.class);
        dossierServiceMock = mock(IDossierService.class);
        serviceSpy = spy(new ClientServiceImpl(clientRepositoryMock, householdServiceMock, diseaseServiceMock, dossierServiceMock));

        client = TestHelpers.createClient();
        clientId = 42L;
    }

    @Test
    public void getAllClients() {
        // arrange
        List<Client> clients = Arrays.asList(client, TestHelpers.createClient());
        when(clientRepositoryMock.findAll()).thenReturn(clients);

        // act
        Iterable<Client> result = serviceSpy.getAllClients();

        // assert
        verify(clientRepositoryMock, times(1)).findAll();
        assertEquals(clients, result);
    }

    @Test
    public void findClientById() {
        // arrange
        when(clientRepositoryMock.findById(anyLong())).thenReturn(Optional.ofNullable(client));

        // act
        Client result = serviceSpy.findClientById(42L);

        // assert
        verify(clientRepositoryMock, times(1)).findById(42L);
        assertEquals(client, result);
    }

    @Test
    public void findClientsByLastName() {
        // arrange
        List<Client> households = Collections.singletonList(client);
        when(clientRepositoryMock.findByLastName(anyString())).thenReturn(households);

        // act
        List<Client> result = serviceSpy.findClientsByLastName("Doe");

        // assert
        verify(clientRepositoryMock, times(1)).findByLastName("Doe");
        assertEquals(households, result);
    }

    @Test
    public void createClient_createsNewClient() {
        // arrange
        client.setParents(List.of(TestHelpers.createClient()));
        client.setId(clientId);
        client.getParents().get(0).setId(clientId);
        when(clientRepositoryMock.save(any(Client.class))).thenReturn(client);

        // act
        Client result = serviceSpy.createClient(client);

        // assert
        verify(diseaseServiceMock, times(0)).createDisease(any(Disease.class));
        verify(dossierServiceMock, times(1)).createDossier(any(Dossier.class));
        verify(serviceSpy, times(1)).createClient(any(Client.class));
        verify(clientRepositoryMock, times(1)).save(client);
        assertEquals(client, result);
    }

    @Test
    public void createClient_recursivelyCreatesParents() {
        // arrange
        Client father = TestHelpers.createClient();
        father.setId(null);
        father.setFirstName("James");
        Client mother = TestHelpers.createClient();
        mother.setId(null);
        mother.setFirstName("Jane");
        client.setParents(Arrays.asList(father, mother));

        when(clientRepositoryMock.save(any(Client.class))).then(i -> i.getArguments()[0]);

        // act
        Client result = serviceSpy.createClient(client);

        // assert
        verify(serviceSpy, times(1)).createClient(any(Client.class));

        ArgumentCaptor<Client> argument = ArgumentCaptor.forClass(Client.class);
        verify(clientRepositoryMock, times(3)).save(argument.capture());
        List<Client> values = argument.getAllValues();
        assertTrue(values.contains(client));
        assertTrue(values.stream().anyMatch(client -> "James".equals(client.getFirstName())));
        assertTrue(values.stream().anyMatch(client -> "Jane".equals(client.getFirstName())));

        assertNotNull(result.getParents());
        assertEquals(2, result.getParents().size());
    }

    @Test
    public void updateClient_returnsClientWhenExisting() {
        // arrange
        client.setId(clientId);
        when(clientRepositoryMock.findById(anyLong())).thenReturn(Optional.ofNullable(client));
        when(clientRepositoryMock.save(any(Client.class))).thenReturn(client);
        // TODO: test for actual updates

        // act
        Client result = serviceSpy.updateClient(client);

        // assert
        verify(clientRepositoryMock, times(1)).findById(42L);
        verify(clientRepositoryMock, times(1)).save(client);
        assertEquals(client, result);
    }

    @Test
    public void updateClient_returnsNullWhenNotExisting() {
        // arrange
        client.setId(clientId);

        // act
        Client result = serviceSpy.updateClient(client);

        // assert
        verify(clientRepositoryMock, times(1)).findById(42L);
        verify(clientRepositoryMock, times(0)).save(client);
        assertNull(result);
    }

    @Test
    public void deleteClient_returnsTrueWhenNotExisting() {
        // arrange
        when(clientRepositoryMock.findById(anyLong())).thenReturn(Optional.empty());

        // act
        boolean result = serviceSpy.deleteClient(42L);

        // assert
        verify(clientRepositoryMock, times(1)).findById(42L);
        verify(clientRepositoryMock, times(0)).delete(client);
        verify(clientRepositoryMock, times(0)).existsById(42L);
        assertTrue(result);
    }

    @Test
    public void deleteClient_returnsTrueOnSuccess() {
        // arrange
        when(clientRepositoryMock.findById(anyLong())).thenReturn(Optional.ofNullable(client));

        // act
        boolean result = serviceSpy.deleteClient(42L);

        // assert
        verify(clientRepositoryMock, times(1)).findById(42L);
        verify(clientRepositoryMock, times(1)).delete(client);
        verify(clientRepositoryMock, times(1)).existsById(42L);
        assertTrue(result);
    }

    @Test
    public void deleteClient_returnsFalseOnFailure() {
        // arrange
        when(clientRepositoryMock.findById(anyLong())).thenReturn(Optional.ofNullable(client));
        when(clientRepositoryMock.existsById(anyLong())).thenReturn(true);

        // act
        boolean result = serviceSpy.deleteClient(42L);

        // assert
        verify(clientRepositoryMock, times(1)).findById(42L);
        verify(clientRepositoryMock, times(1)).delete(client);
        verify(clientRepositoryMock, times(1)).existsById(42L);
        assertFalse(result);
    }
}
