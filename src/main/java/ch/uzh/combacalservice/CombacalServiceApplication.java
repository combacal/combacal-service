package ch.uzh.combacalservice;

import ch.uzh.combacalservice.model.admin.AuthGroups;
import ch.uzh.combacalservice.model.admin.User;
import ch.uzh.combacalservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class CombacalServiceApplication {

    @Autowired
    UserRepository userRepository;

    public static void main(String[] args) {
        SpringApplication.run(CombacalServiceApplication.class, args);
    }

    @PostConstruct
    public void setup() {
        if (userRepository.findAll().isEmpty()) {
            User user = new User();
            user.setUsername("user");
            user.setPassword("any");
            user.setAuthGroups(new ArrayList<>(List.of(AuthGroups.USER)));
            user.setEnabled(true);
            userRepository.save(user);
            User user2 = new User();
            user2.setUsername("admin");
            user2.setPassword("any");
            user2.setAuthGroups(new ArrayList<>(List.of(AuthGroups.ADMIN)));
            user2.setEnabled(true);
            userRepository.save(user2);
        }
    }
}
