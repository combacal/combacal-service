package ch.uzh.combacalservice.security;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * Datatype for authentication requests. Contains all needed information to authenticate
 */
@Data
@AllArgsConstructor
@Builder
public class AuthenticationRequest {

    private String username;
    private String password;
}
