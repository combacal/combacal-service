package ch.uzh.combacalservice.security;

import ch.uzh.combacalservice.exception.CustomAuthenticationException;
import ch.uzh.combacalservice.repository.UserRepository;
import ch.uzh.combacalservice.service.impl.AccessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

/**
 * Authenticates user, different Auth Providers can depict different auth strategies within same project
 */
public class CustomAuthenticationProvider extends DaoAuthenticationProvider {

    @Autowired
    UserRepository userRepository;

    @Autowired
    AccessService accessService;

    @Autowired
    CustomUserDetailsService userDetailsService;

    /**
     * Performs authentication of user information
     *
     * @param authentication User information to authenticate
     * @return Authentication object containing result
     * @throws AuthenticationException if not successful
     */
    // Needs access to identity DB
    // User Details Service -> gets user object, info in user object used for auth
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        // String verificationCode = ((LinkedHashMap<String, String>) authentication.getDetails()).get("username");

        UserDetailsImpl user;
        try {
            user = userDetailsService.loadUserByUsername(authentication.getName());
        } catch (Exception e) {
            throw new CustomAuthenticationException("Username not found");
        }
        Authentication result = super.authenticate(authentication);
        return new UsernamePasswordAuthenticationToken(user.getUsername(), result.getCredentials(), result.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
