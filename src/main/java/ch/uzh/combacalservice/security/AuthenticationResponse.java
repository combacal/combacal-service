package ch.uzh.combacalservice.security;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Response for Auth Request. Contains token if successful
 */
@Data
public class AuthenticationResponse {
    private final String jwt;
}
