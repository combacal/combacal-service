package ch.uzh.combacalservice.model.trainings;

import java.util.List;

public class TrainingQuizStep extends TrainingStep {
    private List<Question> questions;

    private static class Question {
        public String questionText;
        public String answerText;
    }
}
