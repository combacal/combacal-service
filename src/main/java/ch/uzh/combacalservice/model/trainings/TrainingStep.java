package ch.uzh.combacalservice.model.trainings;

import lombok.Getter;

@Getter
public abstract class TrainingStep {
    protected boolean isComplete;
}
