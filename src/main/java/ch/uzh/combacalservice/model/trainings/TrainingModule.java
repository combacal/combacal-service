package ch.uzh.combacalservice.model.trainings;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TrainingModule {
    private List<TrainingStep> steps = new ArrayList<>();
    private String name;
    private Date creationDate;
    private Date modificationDate;
    private Duration refreshInterval;

    public BigDecimal getProgress() {
        return new BigDecimal(0);
    }

    public boolean isComplete() {
        for (TrainingStep step : this.steps) {
            if (!step.isComplete()) {
                return false;
            }
        }
        return true;
    }
}
