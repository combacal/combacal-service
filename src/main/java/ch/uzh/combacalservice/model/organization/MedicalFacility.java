package ch.uzh.combacalservice.model.organization;

import ch.uzh.combacalservice.model.people.Caregiver;
import ch.uzh.combacalservice.model.shared.District;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class MedicalFacility {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany
    private List<Caregiver> caregiver;

    @OneToOne
    private District district;
}
