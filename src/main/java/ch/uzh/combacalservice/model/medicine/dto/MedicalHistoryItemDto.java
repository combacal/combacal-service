package ch.uzh.combacalservice.model.medicine.dto;

import ch.uzh.combacalservice.model.shared.Constants;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
public class MedicalHistoryItemDto {
    private String treatmentDate;

    private List<DiseaseDto> diseases;

    public Date getTreatmentDate() {
        try {
            return new SimpleDateFormat(Constants.DATE_FORMAT).parse(this.treatmentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setTreatmentDate(Date treatmentDate) {
        this.treatmentDate = new SimpleDateFormat(Constants.DATE_FORMAT).format(treatmentDate);
    }
}
