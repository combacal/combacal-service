package ch.uzh.combacalservice.model.medicine;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Medication {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private Integer dosage; // in milligram, milliliter, drops, pills/tablets, ... // TODO CC-158: add prop for unit?

    @Column
    private String sideEffects;

    @Column
    private String intakeInterval;
}
