package ch.uzh.combacalservice.model.medicine;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Treatment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;
}
