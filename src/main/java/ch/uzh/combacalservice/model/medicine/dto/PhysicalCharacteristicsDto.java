package ch.uzh.combacalservice.model.medicine.dto;

import ch.uzh.combacalservice.model.medicine.MedicalRestriction;
import lombok.Data;

import java.util.List;

@Data
public class PhysicalCharacteristicsDto {

    private Integer height; // in centimeters

    private Integer weight; // in gram

    private boolean isPregnantOrBreastfeeding;

    private List<MedicalRestriction> restrictions;
}
