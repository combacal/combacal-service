package ch.uzh.combacalservice.model.medicine;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Disease {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @ManyToMany
    @Column(nullable = false)
    private List<Symptom> symptoms;

    @ManyToMany
    @Column(nullable = false)
    private List<Treatment> treatments;
}
