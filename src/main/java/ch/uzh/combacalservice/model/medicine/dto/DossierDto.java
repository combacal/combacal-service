package ch.uzh.combacalservice.model.medicine.dto;

import ch.uzh.combacalservice.model.medicine.PhysicalCharacteristics;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
public class DossierDto {
    private Date enrollmentDate;

    private String consentRefusalReason;

    private List<PhysicalCharacteristics> physicalCharacteristicsHistory;

    private List<MedicalHistoryItemDto> medicalHistory;
}
