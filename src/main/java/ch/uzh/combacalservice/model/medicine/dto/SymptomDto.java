package ch.uzh.combacalservice.model.medicine.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class SymptomDto {
    private String name;
}
