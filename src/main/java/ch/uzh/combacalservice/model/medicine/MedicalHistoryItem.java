package ch.uzh.combacalservice.model.medicine;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
public class MedicalHistoryItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Date treatmentDate;

    @ManyToMany
    private List<Disease> diseases;
}
