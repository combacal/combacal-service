package ch.uzh.combacalservice.model.medicine;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class PhysicalCharacteristics {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Integer height; // in centimeters

    @Column(nullable = false)
    private Integer weight; // in gram // TODO CC-158: recheck unit

    @Column(nullable = false)
    private boolean isPregnantOrBreastfeeding;

    @OneToMany
    private List<MedicalRestriction> restrictions;

    public boolean hasAllergies() {
        return true;
    }
}
