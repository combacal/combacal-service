package ch.uzh.combacalservice.model.medicine.dto;

import lombok.Data;

@Data
public class MedicationDto {

    private String name;

    private Integer dosage; // in milligram, milliliter, drops, pills/tablets, ... // TODO CC-158: add prop for unit?

    private String sideEffects;

    private String intakeInterval;
}
