package ch.uzh.combacalservice.model.medicine.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
public class DiseaseDto {
    private String name;

    private List<SymptomDto> symptoms;

    private List<TreatmentDto> treatments;
}
