package ch.uzh.combacalservice.model.medicine;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
public class Dossier {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Date enrollmentDate;

    @Column
    private String consentRefusalReason;

    @OneToMany
    private List<PhysicalCharacteristics> physicalCharacteristicsHistory;

    @OneToMany
    private List<MedicalHistoryItem> medicalHistory;
}
