package ch.uzh.combacalservice.model.medicine.dto;

import ch.uzh.combacalservice.model.medicine.Medication;
import lombok.Data;

import java.util.List;

@Data
public class MedicalRestrictionDto {

    private String reason;

    private List<Medication> criticalMedications;

    private String remarks;
}
