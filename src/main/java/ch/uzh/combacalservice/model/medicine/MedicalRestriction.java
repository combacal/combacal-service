package ch.uzh.combacalservice.model.medicine;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class MedicalRestriction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String reason;

    @ManyToMany
    private List<Medication> criticalMedications;

    @Column
    private String remarks;
}
