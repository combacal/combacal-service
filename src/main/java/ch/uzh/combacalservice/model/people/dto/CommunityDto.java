package ch.uzh.combacalservice.model.people.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
public class CommunityDto {
    private List<ClientDto> clients;
}
