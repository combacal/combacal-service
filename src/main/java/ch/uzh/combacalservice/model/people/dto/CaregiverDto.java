package ch.uzh.combacalservice.model.people.dto;

import ch.uzh.combacalservice.model.shared.Gender;
import ch.uzh.combacalservice.model.shared.Profession;
import ch.uzh.combacalservice.model.shared.dto.DistrictDto;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
public class CaregiverDto {
    @Id

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    private String gender;

    private String profession;

    private DistrictDto district;
    private List<CommunityDto> communities;

    public Gender getGender() {
        return Gender.valueOf(this.gender);
    }

    public void setGender(Gender gender) {
        this.gender = gender.toString();
    }

    public Profession getProfession() {
        return Profession.valueOf(this.profession);
    }

    public void setProfession(Profession profession) {
        this.profession = profession.toString();
    }
}
