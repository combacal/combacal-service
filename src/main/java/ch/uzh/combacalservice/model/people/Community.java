package ch.uzh.combacalservice.model.people;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Community {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany
    private List<Client> clients;
}
