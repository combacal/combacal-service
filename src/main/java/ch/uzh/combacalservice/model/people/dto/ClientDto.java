package ch.uzh.combacalservice.model.people.dto;

import ch.uzh.combacalservice.model.medicine.dto.DiseaseDto;
import ch.uzh.combacalservice.model.medicine.dto.DossierDto;
import ch.uzh.combacalservice.model.shared.Constants;
import ch.uzh.combacalservice.model.shared.Gender;
import ch.uzh.combacalservice.model.shared.MaritalStatus;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
public class ClientDto {
    private String firstName;

    private String lastName;

    private String phoneNumber;

    private String gender;

    private String birthDate;

    private MaritalStatus maritalStatus;

    private List<ClientDto> parents;

    private HouseholdDto household;

    private List<DiseaseDto> diseases;

    private DossierDto medicalDossier;

    private boolean isHouseholdRepresentative;

    public Date getBirthDate() {
        try {
            return new SimpleDateFormat(Constants.DATE_FORMAT).parse(this.birthDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = new SimpleDateFormat(Constants.DATE_FORMAT).format(birthDate);
    }

    public Gender getGender() {
        return Gender.valueOf(this.gender);
    }

    public void setGender(Gender gender) {
        this.gender = gender.toString();
    }
}
