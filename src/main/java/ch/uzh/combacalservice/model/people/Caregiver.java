package ch.uzh.combacalservice.model.people;

import ch.uzh.combacalservice.model.shared.District;
import ch.uzh.combacalservice.model.shared.Gender;
import ch.uzh.combacalservice.model.shared.Profession;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Caregiver {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstName;
    private String lastName;

    private Gender gender;

    private Profession profession;

    @OneToOne
    private District district;

    @ManyToMany
    private List<Community> communities;
}
