package ch.uzh.combacalservice.model.people.dto;

import ch.uzh.combacalservice.model.shared.dto.DistrictDto;
import ch.uzh.combacalservice.model.shared.dto.LocationDto;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@EqualsAndHashCode
public class HouseholdDto {
    private String lastName;
    private Date enrollmentDate;
    private DistrictDto district;
    private LocationDto location;
    private String locationDescription;
}
