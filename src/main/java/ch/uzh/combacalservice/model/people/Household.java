package ch.uzh.combacalservice.model.people;

import ch.uzh.combacalservice.model.shared.District;
import ch.uzh.combacalservice.model.shared.Location;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
public class Household {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String lastName;

    @Column(nullable = false)
    private Date enrollmentDate;

    @ManyToOne
    private District district;

    @OneToOne
    private Location location;

    @Column
    private String locationDescription;

    public boolean isRequiringAttention() {
        return false;
    }
}
