package ch.uzh.combacalservice.model.people;

import ch.uzh.combacalservice.model.medicine.Disease;
import ch.uzh.combacalservice.model.medicine.Dossier;
import ch.uzh.combacalservice.model.shared.Gender;
import ch.uzh.combacalservice.model.shared.MaritalStatus;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Column
    private String phoneNumber;

    @Column(nullable = false)
    private Gender gender;

    @Column(nullable = false)
    private Date birthDate;

    @Column(nullable = false)
    private MaritalStatus maritalStatus;

    @OneToMany
    private List<Client> parents;

    @ManyToOne
    private Household household;

    @Column(nullable = false)
    private boolean isHouseholdRepresentative;

    @ManyToMany
    private List<Disease> diseases;

    @OneToOne
    private Dossier medicalDossier;
}
