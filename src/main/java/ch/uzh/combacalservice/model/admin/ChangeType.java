package ch.uzh.combacalservice.model.admin;

/**
 * What kind of change was performed on the data. Used to track changes in database
 */
public enum ChangeType {
    CREATE,
    UPDATE,
    DELETE
}
