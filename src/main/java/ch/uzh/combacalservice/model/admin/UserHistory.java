package ch.uzh.combacalservice.model.admin;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Used to archive changes of users
 */
@Entity
@Data
@NoArgsConstructor
public class UserHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private ChangeType type;

    /**
     * Id of user that was changed
     */
    private Long userId;
    /**
     * Id of admin that performed the changes
     */
    private Long actorId;
    /**
     * Time at which the change was performed
     */
    private Timestamp timestamp;

    /**
     * User information that the user was changed to
     */
    @Column(nullable = false)
    private String username;
    @Column(nullable = false)
    private Boolean enabled;

    @ElementCollection(fetch = FetchType.EAGER)
    @Enumerated(EnumType.STRING)
    private List<AuthGroups> authGroups;

    /**
     * Constructor to migrate information of a user
     *
     * @param user User that should be archived
     */
    public UserHistory(User user) {
        this.userId = user.getId();
        this.username = user.getUsername();
        this.enabled = user.getEnabled();
        this.authGroups = new ArrayList<>();
        authGroups.addAll(user.getAuthGroups() != null ? user.getAuthGroups() : new ArrayList<>());
    }
}
