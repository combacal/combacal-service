package ch.uzh.combacalservice.model.admin;

/**
 * Possible authorisation groups. WebSecurityConfig defines which group has access to which endpoints
 */
public enum AuthGroups {
    /**
     * General User
     */
    USER,
    /**
     * Administrator of the system
     */
    ADMIN
}
