package ch.uzh.combacalservice.model.admin.dto;

import ch.uzh.combacalservice.model.admin.AuthGroups;
import ch.uzh.combacalservice.model.admin.User;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.List;

@Data
public class UserDto {

    private String username;
    @Enumerated(EnumType.STRING)
    private List<AuthGroups> authGroups;
    private Boolean enabled;

    public UserDto(User user) {
        this.username = user.getUsername();
        this.authGroups = user.getAuthGroups();
        this.enabled = user.getEnabled();
    }
}
