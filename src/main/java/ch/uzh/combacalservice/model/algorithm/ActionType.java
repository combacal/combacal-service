package ch.uzh.combacalservice.model.algorithm;

public enum ActionType {
    PRE_SCREENING,
    SCREENING,
    TREATMENT,
    MONITORING
}
