package ch.uzh.combacalservice.model.algorithm;

import lombok.Getter;

@Getter
public class ActionStep {
    private boolean isDone;
}
