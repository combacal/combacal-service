package ch.uzh.combacalservice.model.algorithm;

import java.util.Date;
import java.util.List;

public abstract class ActionItem {
    protected Date dueDate;
    protected Date finishDate;

    protected ActionType type;

    protected List<ActionStep> steps;

    protected Long clientId;

    protected boolean isDone() {
        for (ActionStep step : this.steps) {
            if (!step.isDone()) {
                return false;
            }
        }
        return true;
    }
}
