package ch.uzh.combacalservice.model.algorithm;

public class TreatmentTree extends ActionItem {
    protected ActionType type = ActionType.TREATMENT;

    public boolean isCurrent() {
        // new treatment can replace old treatment, e.g., with adjusted medication
        return true;
    }
}
