package ch.uzh.combacalservice.model.shared.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class DistrictDto {
    private String name;
}
