package ch.uzh.combacalservice.model.shared.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class LocationDto {
    private String latitude;
    private String longitude;
}
