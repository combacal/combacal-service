package ch.uzh.combacalservice.model.shared;

public enum MaritalStatus {
    MARRIED,
    SINGLE,
    WIDOWED,
    DIVORCED
}
