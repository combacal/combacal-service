package ch.uzh.combacalservice.model.shared;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class District {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;
}
