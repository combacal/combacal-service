package ch.uzh.combacalservice.model.shared;

public enum Gender {
    MALE,
    FEMALE
}
