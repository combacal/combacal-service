package ch.uzh.combacalservice.model.shared;

public class Constants {
    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final Integer TOKEN_VALIDITY = 1000 * 60 * 60 * 10;
}
