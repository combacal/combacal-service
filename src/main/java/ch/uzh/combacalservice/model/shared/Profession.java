package ch.uzh.combacalservice.model.shared;

public enum Profession {
    DOCTOR,
    LAY_HEALTH_WORKER,
    NURSE
}
