package ch.uzh.combacalservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class RequestParameterMismatchException extends CustomBaseException {
    public RequestParameterMismatchException(String pathVariableName, Object pathVariableValue, String requestBodyName, Object requestBodyValue) {
        super(String.format("Provided request path variable '%s:%s' does not match request body '%s:%s'", pathVariableName, pathVariableValue, requestBodyName, requestBodyValue));
    }
}
