package ch.uzh.combacalservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends CustomBaseException {
    public ResourceNotFoundException(Class resourceType, String fieldName, Object fieldValue) {
        super(String.format("Resource of type %s not found with %s : '%s'", resourceType.getSimpleName(), fieldName, fieldValue));
    }
}
