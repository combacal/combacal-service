package ch.uzh.combacalservice.exception;

public class CustomBaseException extends RuntimeException {
    public CustomBaseException(String reason) {
        super(reason);
        this.printStackTrace();
    }
}
