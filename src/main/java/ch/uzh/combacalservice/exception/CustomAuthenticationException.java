package ch.uzh.combacalservice.exception;

/**
 * Exception that is thrown if access cannot be granted to user trying to authorise
 */
public class CustomAuthenticationException extends RuntimeException {
    public CustomAuthenticationException(String msg, Throwable t) {
        super(msg, t);
    }

    public CustomAuthenticationException(String msg) {
        super(msg);
    }
}
