package ch.uzh.combacalservice.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Schema containing relevant fields for custom exceptions
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GeneralExceptionSchema {
    /**
     * Short description of the problem
     */
    private String message;
    /**
     * More detailed description of the problem
     */
    private String details;
    /**
     * Code as reference, unique per context
     */
    private String errorCode;
    /**
     * Stacktrace of error
     */
    private String trace;
}

