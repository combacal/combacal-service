package ch.uzh.combacalservice.config;

import ch.uzh.combacalservice.security.CustomAuthenticationProvider;
import ch.uzh.combacalservice.security.CustomUserDetailsService;
import ch.uzh.combacalservice.security.JwtRequestFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.sql.DataSource;

/**
 * Configuration that handles Security. Defines interception of requests, authorisation, authentication
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    DataSource dataSource;

    @Autowired
    CustomUserDetailsService userDetailsService;

    @Autowired
    JwtRequestFilter filter;

    /**
     * Needs to be explicitly called, otherwise AuthManager cannot be found
     *
     * @return AuthManager
     * @throws Exception if not present
     */
    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    /**
     * Defines datasource (DB) and which tables the information can be found in
     *
     * @param builder to be extended/ adapted to new context
     * @throws Exception
     */
    @Override
    public void configure(AuthenticationManagerBuilder builder) throws Exception {

        builder.userDetailsService(userDetailsService);
        builder.jdbcAuthentication().dataSource(dataSource)
                .usersByUsernameQuery("select username,password, enabled "
                        + "from user "
                        + "where username = ?")
                .authoritiesByUsernameQuery("select username,auth_group "
                        + "from user "
                        + "where username = ?");
    }

    /**
     * Defines which endpoints have what kind of security
     *
     * @param http to be extended/ adapted to new context
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and() // Stateless because Tokenbased
                .authorizeRequests()
                .antMatchers("/admin").hasAuthority("ADMIN") // Has to be authority (not role)
                .antMatchers("/user").hasAnyAuthority("USER", "ADMIN")
                .antMatchers("/user/**").hasAnyAuthority("USER", "ADMIN")
                .antMatchers("/").permitAll()
                .antMatchers("/login").permitAll()
                .antMatchers("/login/**").permitAll()
                .antMatchers("/**").hasAuthority("ADMIN")
                .anyRequest().authenticated();
        // Call custom filter before username password filter
        http.addFilterBefore(filter, UsernamePasswordAuthenticationFilter.class); // Add Token Filter
    }

    /**
     * Bean for custom auth provider
     *
     * @return Custom auth provider
     */
    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        CustomAuthenticationProvider authProvider = new CustomAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService);
        authProvider.setPasswordEncoder(getPasswordEncoder());
        return authProvider;
    }

    /**
     * Defines how the password is encoded. Currently empty encoder. TODO: change to real encoder
     *
     * @return Encoder
     */
    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return NoOpPasswordEncoder.getInstance(); // TODO: CC-163 Only for Tutorial! Does not actually encode
    }
}
