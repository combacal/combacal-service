package ch.uzh.combacalservice.config;

import ch.uzh.combacalservice.exception.CustomAuthenticationException;
import ch.uzh.combacalservice.exception.GeneralExceptionSchema;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionInterceptor extends ResponseEntityExceptionHandler {

    /**
     * Handles all CustomAuthenticationExceptions
     *
     * @param ex Exception that is thrown
     * @return Response with error code 500
     */
    @ExceptionHandler(CustomAuthenticationException.class)
    public final ResponseEntity<Object> handleAllExceptions(CustomAuthenticationException ex) {
        GeneralExceptionSchema exceptionResponse =
                GeneralExceptionSchema.builder().message(ex.getMessage()).build();
        return new ResponseEntity(exceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
