package ch.uzh.combacalservice.service;

import ch.uzh.combacalservice.model.admin.dto.UserDto;
import ch.uzh.combacalservice.security.AuthenticationRequest;

public interface IAdminService {
    UserDto createUser(AuthenticationRequest request, String principal);

    UserDto createAdmin(AuthenticationRequest request, String principal);

    void disableUser(AuthenticationRequest request, String principal);

    void enableUser(AuthenticationRequest request, String principal);
}
