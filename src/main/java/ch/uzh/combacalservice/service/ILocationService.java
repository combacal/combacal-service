package ch.uzh.combacalservice.service;

import ch.uzh.combacalservice.model.shared.Location;
import org.springframework.stereotype.Service;

@Service
public interface ILocationService {
    Location findLocationById(Long id);

    Location createLocation(Location location);

    Location updateLocation(Location location);
}
