package ch.uzh.combacalservice.service;

import ch.uzh.combacalservice.model.admin.User;

public interface IAccessService {
    User getUserByUsername(String username);

    User getUserById(Long id);

    User addUser(User user, String principal);

    User updateUser(User user, String principal);
}
