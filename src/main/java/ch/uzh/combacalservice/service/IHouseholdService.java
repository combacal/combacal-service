package ch.uzh.combacalservice.service;

import ch.uzh.combacalservice.model.people.Household;
import org.springframework.stereotype.Service;

@Service
public interface IHouseholdService {
    Iterable<Household> getAllHouseholds();

    Household findHouseholdById(Long id);

    Iterable<Household> findHouseholdsByLastName(String lastName);

    Household createHousehold(Household household);

    Household updateHousehold(Household household);

    boolean deleteHousehold(Long householdId);
}
