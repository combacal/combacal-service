package ch.uzh.combacalservice.service;

import ch.uzh.combacalservice.model.medicine.Dossier;
import org.springframework.stereotype.Service;

@Service
public interface IDossierService {
    Dossier findDossierById(Long id);

    Dossier createDossier(Dossier dossier);

    Dossier updateDossier(Dossier dossier);
}
