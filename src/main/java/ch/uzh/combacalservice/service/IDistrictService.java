package ch.uzh.combacalservice.service;

import ch.uzh.combacalservice.model.shared.District;
import org.springframework.stereotype.Service;

@Service
public interface IDistrictService {
    District findDistrictById(Long id);

    District createDistrict(District district);

    District updateDistrict(District district);
}
