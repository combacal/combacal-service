package ch.uzh.combacalservice.service.impl;

import ch.uzh.combacalservice.model.medicine.MedicalHistoryItem;
import ch.uzh.combacalservice.repository.IMedicalHistoryItemRepository;
import ch.uzh.combacalservice.service.IDiseaseService;
import ch.uzh.combacalservice.service.IMedicalHistoryItemService;
import lombok.RequiredArgsConstructor;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MedicalHistoryItemServiceImpl implements IMedicalHistoryItemService {
    private final IMedicalHistoryItemRepository medicalHistoryItemRepository;
    private final IDiseaseService diseaseService;

    @Nullable
    public MedicalHistoryItem findMedicalHistoryItemById(Long id) {
        return medicalHistoryItemRepository.findById(id).orElse(null);
    }

    public MedicalHistoryItem createMedicalHistoryItem(MedicalHistoryItem medicalHistoryItem) {
        return medicalHistoryItemRepository.save(medicalHistoryItem);
    }

    @Nullable
    public MedicalHistoryItem updateMedicalHistoryItem(MedicalHistoryItem medicalHistoryItem) {
        MedicalHistoryItem existingMedicalHistoryItem = medicalHistoryItemRepository.findById(medicalHistoryItem.getId()).orElse(null);
        if (null == existingMedicalHistoryItem) {
            return null;
        }
        if (!existingMedicalHistoryItem.getDiseases().equals(medicalHistoryItem.getDiseases())) {
            medicalHistoryItem.setDiseases(medicalHistoryItem.getDiseases().stream().map(disease -> {
                if (null == diseaseService.findDiseaseById(disease.getId())) {
                    return diseaseService.createDisease(disease);
                }
                return diseaseService.updateDisease(disease);
            }).collect(Collectors.toList()));
        }
        return medicalHistoryItemRepository.save(medicalHistoryItem);
    }
}
