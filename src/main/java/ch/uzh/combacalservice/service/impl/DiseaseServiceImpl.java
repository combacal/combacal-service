package ch.uzh.combacalservice.service.impl;

import ch.uzh.combacalservice.model.medicine.Disease;
import ch.uzh.combacalservice.repository.IDiseaseRepository;
import ch.uzh.combacalservice.service.IDiseaseService;
import ch.uzh.combacalservice.service.ISymptomService;
import ch.uzh.combacalservice.service.ITreatmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DiseaseServiceImpl implements IDiseaseService {
    private final IDiseaseRepository diseaseRepository;
    private final ISymptomService symptomService;
    private final ITreatmentService treatmentService;

    @Nullable
    public Disease findDiseaseById(Long id) {
        return diseaseRepository.findById(id).orElse(null);
    }

    public Disease createDisease(Disease disease) {
        disease.setSymptoms(disease.getSymptoms().stream().map(symptomService::createSymptom).collect(Collectors.toList()));
        disease.setTreatments(disease.getTreatments().stream().map(treatmentService::createTreatment).collect(Collectors.toList()));
        return diseaseRepository.save(disease);
    }

    @Nullable
    public Disease updateDisease(Disease disease) {
        Disease existingDisease = diseaseRepository.findById(disease.getId()).orElse(null);
        if (null == existingDisease) {
            return null;
        }
        if (!existingDisease.getSymptoms().equals(disease.getSymptoms())) {
            disease.setSymptoms(disease.getSymptoms().stream().map(symptom -> {
                if (null == symptomService.findSymptomById(symptom.getId())) {
                    return symptomService.createSymptom(symptom);
                }
                return symptomService.updateSymptom(symptom);
            }).collect(Collectors.toList()));
        }
        if (!existingDisease.getTreatments().equals(disease.getTreatments())) {
            disease.setTreatments(disease.getTreatments().stream().map(treatment -> {
                if (null == treatmentService.findTreatmentById(treatment.getId())) {
                    return treatmentService.createTreatment(treatment);
                }
                return treatmentService.updateTreatment(treatment);
            }).collect(Collectors.toList()));
        }
        return diseaseRepository.save(disease);
    }
}
