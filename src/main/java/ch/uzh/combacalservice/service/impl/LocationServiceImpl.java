package ch.uzh.combacalservice.service.impl;

import ch.uzh.combacalservice.model.shared.Location;
import ch.uzh.combacalservice.repository.ILocationRepository;
import ch.uzh.combacalservice.service.ILocationService;
import lombok.RequiredArgsConstructor;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LocationServiceImpl implements ILocationService {
    private final ILocationRepository locationRepository;

    @Nullable
    public Location findLocationById(Long id) {
        return locationRepository.findById(id).orElse(null);
    }

    public Location createLocation(Location location) {
        return locationRepository.save(location);
    }

    @Nullable
    public Location updateLocation(Location location) {
        Location existingLocation = locationRepository.findById(location.getId()).orElse(null);
        if (null == existingLocation) {
            return null;
        }
        return locationRepository.save(location);
    }
}
