package ch.uzh.combacalservice.service.impl;

import ch.uzh.combacalservice.model.people.Household;
import ch.uzh.combacalservice.model.shared.District;
import ch.uzh.combacalservice.model.shared.Location;
import ch.uzh.combacalservice.repository.IHouseholdRepository;
import ch.uzh.combacalservice.service.IDistrictService;
import ch.uzh.combacalservice.service.IHouseholdService;
import ch.uzh.combacalservice.service.ILocationService;
import com.sun.istack.Nullable;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class HouseholdServiceImpl implements IHouseholdService {
    private final IHouseholdRepository householdRepository;
    private final IDistrictService districtService;
    private final ILocationService locationService;

    public Iterable<Household> getAllHouseholds() {
        return householdRepository.findAll();
    }

    @Nullable
    public Household findHouseholdById(Long id) {
        return householdRepository.findById(id).orElse(null);
    }

    public List<Household> findHouseholdsByLastName(String lastName) {
        return householdRepository.findByLastName(lastName);
    }

    public Household createHousehold(Household household) {
        household.setDistrict(districtService.createDistrict(household.getDistrict()));
        return householdRepository.save(household);
    }

    @Nullable
    public Household updateHousehold(Household household) {
        Household existingHousehold = householdRepository.findById(household.getId()).orElse(null);
        if (null == existingHousehold) {
            return null;
        }
        if (!existingHousehold.getDistrict().equals(household.getDistrict())) {
            District district = null == districtService.findDistrictById(household.getDistrict().getId())
                    ? districtService.createDistrict(household.getDistrict())
                    : districtService.updateDistrict(household.getDistrict());
            household.setDistrict(district);
        }
        if (!existingHousehold.getLocation().equals(household.getLocation())) {
            Location location = null == locationService.findLocationById(household.getLocation().getId())
                    ? locationService.createLocation(household.getLocation())
                    : locationService.updateLocation(household.getLocation());
            household.setLocation(location);
        }
        return householdRepository.save(household);
    }

    public boolean deleteHousehold(Long householdId) {
        Optional<Household> existingHousehold = householdRepository.findById(householdId);
        if (existingHousehold.isPresent()) {
            householdRepository.delete(existingHousehold.get());
            return !householdRepository.existsById(householdId);
        }
        return true; // nothing to delete
    }
}
