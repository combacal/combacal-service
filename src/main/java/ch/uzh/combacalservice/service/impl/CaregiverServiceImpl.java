package ch.uzh.combacalservice.service.impl;

import ch.uzh.combacalservice.model.people.Caregiver;
import ch.uzh.combacalservice.model.shared.District;
import ch.uzh.combacalservice.repository.ICaregiverRepository;
import ch.uzh.combacalservice.service.ICaregiverService;
import ch.uzh.combacalservice.service.ICommunityService;
import ch.uzh.combacalservice.service.IDistrictService;
import lombok.RequiredArgsConstructor;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CaregiverServiceImpl implements ICaregiverService {
    private final ICaregiverRepository caregiverRepository;
    private final IDistrictService districtService;
    private final ICommunityService communityService;

    public Iterable<Caregiver> getAllCaregivers() {
        return caregiverRepository.findAll();
    }

    @Nullable
    public Caregiver findCaregiverById(Long id) {
        return caregiverRepository.findById(id).orElse(null);
    }

    public Caregiver createCaregiver(Caregiver caregiver) {
        caregiver.setDistrict(districtService.createDistrict(caregiver.getDistrict()));
        caregiver.setCommunities(caregiver.getCommunities().stream().map(communityService::createCommunity).collect(Collectors.toList()));
        return caregiverRepository.save(caregiver);
    }

    @Nullable
    public Caregiver updateCaregiver(Caregiver caregiver) {
        Caregiver existingCaregiver = caregiverRepository.findById(caregiver.getId()).orElse(null);
        if (null == existingCaregiver) {
            return null;
        }
        if (!existingCaregiver.getDistrict().equals(caregiver.getDistrict())) {
            District district = null == districtService.findDistrictById(caregiver.getDistrict().getId())
                    ? districtService.createDistrict(caregiver.getDistrict())
                    : districtService.updateDistrict(caregiver.getDistrict());
            caregiver.setDistrict(district);
        }
        if (!existingCaregiver.getCommunities().equals(caregiver.getCommunities())) {
            caregiver.setCommunities(caregiver.getCommunities().stream().map(community -> {
                if (null == communityService.findCommunityById(community.getId())) {
                    return communityService.createCommunity(community);
                }
                return communityService.updateCommunity(community);
            }).collect(Collectors.toList()));
        }
        return caregiverRepository.save(caregiver);
    }
}
