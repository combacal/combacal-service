package ch.uzh.combacalservice.service.impl;

import ch.uzh.combacalservice.model.shared.District;
import ch.uzh.combacalservice.repository.IDistrictRepository;
import ch.uzh.combacalservice.service.IDistrictService;
import lombok.RequiredArgsConstructor;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DistrictServiceImpl implements IDistrictService {
    private final IDistrictRepository districtRepository;

    @Nullable
    public District findDistrictById(Long id) {
        return districtRepository.findById(id).orElse(null);
    }

    public District createDistrict(District district) {
        return districtRepository.save(district);
    }

    @Nullable
    public District updateDistrict(District district) {
        District existingDistrict = districtRepository.findById(district.getId()).orElse(null);
        if (null == existingDistrict) {
            return null;
        }
        return districtRepository.save(district);
    }
}
