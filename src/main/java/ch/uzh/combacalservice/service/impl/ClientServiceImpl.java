package ch.uzh.combacalservice.service.impl;

import ch.uzh.combacalservice.model.medicine.Dossier;
import ch.uzh.combacalservice.model.people.Client;
import ch.uzh.combacalservice.model.people.Household;
import ch.uzh.combacalservice.repository.IClientRepository;
import ch.uzh.combacalservice.service.IClientService;
import ch.uzh.combacalservice.service.IDiseaseService;
import ch.uzh.combacalservice.service.IDossierService;
import ch.uzh.combacalservice.service.IHouseholdService;
import com.sun.istack.Nullable;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ClientServiceImpl implements IClientService {
    private final IClientRepository clientRepository;
    private final IHouseholdService householdService;
    private final IDiseaseService diseaseService;
    private final IDossierService dossierService;

    public Iterable<Client> getAllClients() {
        return clientRepository.findAll();
    }

    @Nullable
    public Client findClientById(Long id) {
        return clientRepository.findById(id).orElse(null);
    }

    public List<Client> findClientsByLastName(String lastName) {
        return clientRepository.findByLastName(lastName);
    }

    public Client createClient(Client client) {
        return this.saveClient(client); // TODO: refactor to include fromDto in method below and then move content back in here, recurse with Dto
    }

    private Client saveClient(Client client) {
        client.setMedicalDossier(dossierService.createDossier(client.getMedicalDossier()));
        client.setDiseases(client.getDiseases().stream().map(diseaseService::createDisease).collect(Collectors.toList()));

        var parents = client.getParents().stream().map(parent -> {
            if (null == parent.getId()) { // TODO: implement @Nullable frontendId and findByFrontendId() logic to avoid this mess here
                return this.saveClient(parent);
            }
            return parent;
        }).collect(Collectors.toList());
        client.setParents(parents);

        return clientRepository.save(client);
    }

    @Nullable
    public Client updateClient(Client client) {
        Client existingClient = clientRepository.findById(client.getId()).orElse(null);
        if (null == existingClient) {
            return null;
        }
        if (!existingClient.getParents().equals(client.getParents())) {
            client.setParents(client.getParents().stream().map(parent -> {
                if (!clientRepository.existsById(parent.getId())) {
                    return this.createClient(parent);
                }
                return this.updateClient(parent);
            }).collect(Collectors.toList()));
        }
        if (!existingClient.getHousehold().equals(client.getHousehold())) {
            Household household = null == householdService.findHouseholdById(client.getHousehold().getId())
                    ? householdService.createHousehold(client.getHousehold())
                    : householdService.updateHousehold(client.getHousehold());
            client.setHousehold(household);
        }
        if (!existingClient.getDiseases().equals(client.getDiseases())) {
            client.setDiseases(client.getDiseases().stream().map(disease -> {
                if (null == diseaseService.findDiseaseById(disease.getId())) {
                    return diseaseService.createDisease(disease);
                }
                return diseaseService.updateDisease(disease);
            }).collect(Collectors.toList()));
        }
        if (!existingClient.getMedicalDossier().equals(client.getMedicalDossier())) {
            Dossier dossier = null == dossierService.findDossierById(client.getMedicalDossier().getId())
                    ? dossierService.createDossier(client.getMedicalDossier())
                    : dossierService.updateDossier(client.getMedicalDossier());
            client.setMedicalDossier(dossier);
        }
        return clientRepository.save(client);
    }

    public boolean deleteClient(Long clientId) {
        Optional<Client> existingClient = clientRepository.findById(clientId);
        if (existingClient.isPresent()) {
            clientRepository.delete(existingClient.get());
            return !clientRepository.existsById(clientId);
        }
        return true; // nothing to delete
    }
}
