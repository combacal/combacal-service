package ch.uzh.combacalservice.service.impl;

import ch.uzh.combacalservice.model.admin.AuthGroups;
import ch.uzh.combacalservice.model.admin.User;
import ch.uzh.combacalservice.model.admin.dto.UserDto;
import ch.uzh.combacalservice.security.AuthenticationRequest;
import ch.uzh.combacalservice.service.IAccessService;
import ch.uzh.combacalservice.service.IAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Provides Functionality to admins
 */
@Service
public class AdminService implements IAdminService {


    IAccessService accessService;

    @Autowired
    AdminService(IAccessService accessService) {
        this.accessService = accessService;
    }

    /**
     * Creates a new user and creates a history element for tracking.
     * If user already exists or input is not complete throws error.
     *
     * @param request   Contains information about the user to be created
     * @param principal Username of user that performs the creation.
     * @return DTO Information about created user
     */
    @Override
    public UserDto createUser(AuthenticationRequest request, String principal) {
        User user = new User();
        user.setUsername(request.getUsername());
        user.setPassword(request.getPassword());
        user.setEnabled(true);
        user.setAuthGroups(new ArrayList<>(List.of(AuthGroups.USER)));
        user = accessService.addUser(user, principal);
        return new UserDto(user);
    }

    /**
     * Creates a new admin and creates a history element for tracking.
     * If user already exists or input is not complete throws error.
     *
     * @param request   Contains information about the user to be created
     * @param principal Username of user that performs the creation.
     * @return DTO Information about created admin
     */
    @Override
    public UserDto createAdmin(AuthenticationRequest request, String principal) {
        User user = new User();
        user.setUsername(request.getUsername());
        user.setPassword(request.getPassword());
        user.setEnabled(true);
        user.setAuthGroups(new ArrayList<>(List.of(AuthGroups.ADMIN)));
        user = accessService.addUser(user, principal);
        return new UserDto(user);
    }

    /**
     * Disables the user provided in the request
     *
     * @param request   User to be disabled
     * @param principal Admin performing action
     */
    @Override
    public void disableUser(AuthenticationRequest request, String principal) {
        User user = accessService.getUserByUsername(request.getUsername());
        user.setEnabled(false);
        accessService.updateUser(user, principal);
    }

    /**
     * Enables the user provided in the request
     *
     * @param request   User to be enabled
     * @param principal Admin performing action
     */
    @Override
    public void enableUser(AuthenticationRequest request, String principal) {
        User user = accessService.getUserByUsername(request.getUsername());
        user.setEnabled(true);
        accessService.updateUser(user, principal);
    }
}
