package ch.uzh.combacalservice.service.impl;

import ch.uzh.combacalservice.model.people.Community;
import ch.uzh.combacalservice.repository.ICommunityRepository;
import ch.uzh.combacalservice.service.IClientService;
import ch.uzh.combacalservice.service.ICommunityService;
import lombok.RequiredArgsConstructor;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CommunityServiceImpl implements ICommunityService {
    private final ICommunityRepository communityRepository;
    private final IClientService clientService;

    public Iterable<Community> getAllCommunities() {
        return communityRepository.findAll();
    }

    @Nullable
    public Community findCommunityById(Long id) {
        return communityRepository.findById(id).orElse(null);
    }

    public Community createCommunity(Community community) {
        return communityRepository.save(community);
    }

    @Nullable
    public Community updateCommunity(Community community) {
        Community existingCommunity = communityRepository.findById(community.getId()).orElse(null);
        if (null == existingCommunity) {
            return null;
        }
        if (!existingCommunity.getClients().equals(community.getClients())) {
            community.setClients(community.getClients().stream().map(client -> {
                if (null == clientService.findClientById(client.getId())) {
                    return clientService.createClient(client);
                }
                return clientService.updateClient(client);
            }).collect(Collectors.toList()));
        }
        return communityRepository.save(community);
    }
}
