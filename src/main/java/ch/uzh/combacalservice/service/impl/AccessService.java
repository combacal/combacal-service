package ch.uzh.combacalservice.service.impl;

import ch.uzh.combacalservice.exception.CustomAuthenticationException;
import ch.uzh.combacalservice.model.admin.ChangeType;
import ch.uzh.combacalservice.model.admin.User;
import ch.uzh.combacalservice.model.admin.UserHistory;
import ch.uzh.combacalservice.repository.UserHistoryRepository;
import ch.uzh.combacalservice.repository.UserRepository;
import ch.uzh.combacalservice.service.IAccessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Optional;

/**
 * Handles all interaction with the user repository
 */
@Service
public class AccessService implements IAccessService {


    UserRepository userRepository;
    UserHistoryRepository userHistoryRepository;

    @Autowired
    AccessService(UserRepository userRepository,
                  UserHistoryRepository userHistoryRepository) {
        this.userRepository = userRepository;
        this.userHistoryRepository = userHistoryRepository;
    }

    /**
     * Retrieves user from database with username
     *
     * @param username of user to be searched for
     * @return User object, if present
     */
    @Override
    public User getUserByUsername(String username) {
        Optional<User> user = userRepository.findByUsername(username);
        if (user.isPresent()) {
            return user.get();
        }
        throw new CustomAuthenticationException("User does not exist");
    }

    /**
     * Retrieves user from database with id
     *
     * @param id of user to be searched for
     * @return User object, if present
     */
    @Override
    public User getUserById(Long id) {
        Optional<User> user = userRepository.findById(id);
        if (user.isPresent()) {
            return user.get();
        }
        throw new CustomAuthenticationException("User does not exist");
    }

    /**
     * Add new user to system.
     *
     * @param user      User information to be added
     * @param principal username of admin performing the request
     * @return User after being added
     */
    @Override
    public User addUser(User user, String principal) {
        User tempUser = user;
        if (tempUser.getUsername() == null || tempUser.getPassword() == null) {
            throw new CustomAuthenticationException("Username and password cannot be null");
        }
        try {
            getUserByUsername(tempUser.getUsername());
        } catch (Exception ignored) {
            tempUser = userRepository.save(tempUser);
            UserHistory hist = new UserHistory(user);
            hist.setType(ChangeType.CREATE);
            hist.setActorId(getUserByUsername(principal).getId());
            hist.setTimestamp(Timestamp.from(Instant.now()));
            userHistoryRepository.save(hist);
            return tempUser;
        }
        throw new CustomAuthenticationException("User already exists");
    }

    /**
     * Method to update username, enabled, password and authgroups
     *
     * @param user      User with changed fields already.
     * @param principal User performing the changes
     * @return Updated user
     */
    @Override
    public User updateUser(User user, String principal) {
        // assert that user exists
        User current = getUserById(user.getId()); // is password null?
        // change the attributes if not empty
        if (user.getUsername() != null) {
            current.setUsername(user.getUsername());
        }
        if (user.getEnabled() != null) {
            current.setEnabled(user.getEnabled());
        }
        if (user.getPassword() != null) {
            current.setPassword(user.getPassword());
        }
        if (user.getAuthGroups() != null) {
            current.setAuthGroups(user.getAuthGroups());
        }
        // prepare history
        UserHistory hist = new UserHistory(user);
        hist.setType(ChangeType.UPDATE);
        hist.setActorId(getUserByUsername(principal).getId());
        hist.setTimestamp(Timestamp.from(Instant.now()));
        userHistoryRepository.save(hist);
        return userRepository.save(current);
    }
}
