package ch.uzh.combacalservice.service.impl;

import ch.uzh.combacalservice.model.medicine.Treatment;
import ch.uzh.combacalservice.repository.ITreatmentRepository;
import ch.uzh.combacalservice.service.ITreatmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TreatmentServiceImpl implements ITreatmentService {
    private final ITreatmentRepository treatmentRepository;

    @Nullable
    public Treatment findTreatmentById(Long id) {
        return treatmentRepository.findById(id).orElse(null);
    }

    public Treatment createTreatment(Treatment treatment) {
        return treatmentRepository.save(treatment);
    }

    @Nullable
    public Treatment updateTreatment(Treatment treatment) {
        Treatment existingTreatment = treatmentRepository.findById(treatment.getId()).orElse(null);
        if (null == existingTreatment) {
            return null;
        }
        return treatmentRepository.save(treatment);
    }
}
