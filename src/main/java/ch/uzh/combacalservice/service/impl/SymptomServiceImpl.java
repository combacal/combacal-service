package ch.uzh.combacalservice.service.impl;

import ch.uzh.combacalservice.model.medicine.Symptom;
import ch.uzh.combacalservice.repository.ISymptomRepository;
import ch.uzh.combacalservice.service.ISymptomService;
import lombok.RequiredArgsConstructor;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SymptomServiceImpl implements ISymptomService {
    private final ISymptomRepository symptomRepository;

    @Nullable
    public Symptom findSymptomById(Long id) {
        return symptomRepository.findById(id).orElse(null);
    }

    public Symptom createSymptom(Symptom symptom) {
        return symptomRepository.save(symptom);
    }

    @Nullable
    public Symptom updateSymptom(Symptom symptom) {
        Symptom existingSymptom = symptomRepository.findById(symptom.getId()).orElse(null);
        if (null == existingSymptom) {
            return null;
        }
        return symptomRepository.save(symptom);
    }
}
