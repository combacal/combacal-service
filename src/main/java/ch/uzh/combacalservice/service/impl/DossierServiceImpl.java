package ch.uzh.combacalservice.service.impl;

import ch.uzh.combacalservice.model.medicine.Dossier;
import ch.uzh.combacalservice.repository.IDossierRepository;
import ch.uzh.combacalservice.service.IDossierService;
import ch.uzh.combacalservice.service.IMedicalHistoryItemService;
import lombok.RequiredArgsConstructor;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DossierServiceImpl implements IDossierService {
    private final IDossierRepository dossierRepository;
    private final IMedicalHistoryItemService medicalHistoryItemService;

    @Nullable
    public Dossier findDossierById(Long id) {
        return dossierRepository.findById(id).orElse(null);
    }

    public Dossier createDossier(Dossier dossier) {
        return dossierRepository.save(dossier);
    }

    @Nullable
    public Dossier updateDossier(Dossier dossier) {
        Dossier existingDossier = dossierRepository.findById(dossier.getId()).orElse(null);
        if (null == existingDossier) {
            return null;
        }
        if (!existingDossier.getMedicalHistory().equals(dossier.getMedicalHistory())) {
            dossier.setMedicalHistory(dossier.getMedicalHistory().stream().map(medicalHistoryItem -> {
                if (null == medicalHistoryItemService.findMedicalHistoryItemById(medicalHistoryItem.getId())) {
                    return medicalHistoryItemService.createMedicalHistoryItem(medicalHistoryItem);
                }
                return medicalHistoryItemService.updateMedicalHistoryItem(medicalHistoryItem);
            }).collect(Collectors.toList()));
        }
        return dossierRepository.save(dossier);
    }
}
