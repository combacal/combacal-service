package ch.uzh.combacalservice.service;

import ch.uzh.combacalservice.model.medicine.Disease;
import org.springframework.stereotype.Service;

@Service
public interface IDiseaseService {
    Disease findDiseaseById(Long id);

    Disease createDisease(Disease disease);

    Disease updateDisease(Disease disease);
}
