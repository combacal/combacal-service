package ch.uzh.combacalservice.service;

import ch.uzh.combacalservice.model.medicine.Treatment;
import org.springframework.stereotype.Service;

@Service
public interface ITreatmentService {
    Treatment findTreatmentById(Long id);

    Treatment createTreatment(Treatment treatment);

    Treatment updateTreatment(Treatment treatment);
}
