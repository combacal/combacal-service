package ch.uzh.combacalservice.service;

import ch.uzh.combacalservice.model.medicine.MedicalHistoryItem;
import org.springframework.stereotype.Service;

@Service
public interface IMedicalHistoryItemService {
    MedicalHistoryItem findMedicalHistoryItemById(Long id);

    MedicalHistoryItem createMedicalHistoryItem(MedicalHistoryItem medicalHistoryItem);

    MedicalHistoryItem updateMedicalHistoryItem(MedicalHistoryItem medicalHistoryItem);
}
