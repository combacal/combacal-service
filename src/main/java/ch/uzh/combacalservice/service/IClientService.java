package ch.uzh.combacalservice.service;

import ch.uzh.combacalservice.model.people.Client;
import org.springframework.stereotype.Service;

@Service
public interface IClientService {

    Iterable<Client> getAllClients();

    Client findClientById(Long id);

    Iterable<Client> findClientsByLastName(String lastName);

    Client createClient(Client newClient);

    Client updateClient(Client client);

    boolean deleteClient(Long clientId);
}
