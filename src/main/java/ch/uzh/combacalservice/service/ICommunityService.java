package ch.uzh.combacalservice.service;

import ch.uzh.combacalservice.model.people.Community;
import org.springframework.stereotype.Service;

@Service
public interface ICommunityService {
    Iterable<Community> getAllCommunities();

    Community findCommunityById(Long id);

    Community createCommunity(Community community);

    Community updateCommunity(Community community);
}
