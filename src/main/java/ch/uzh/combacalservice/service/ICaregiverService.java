package ch.uzh.combacalservice.service;

import ch.uzh.combacalservice.model.people.Caregiver;
import org.springframework.stereotype.Service;

public interface ICaregiverService {
    Iterable<Caregiver> getAllCaregivers();

    Caregiver findCaregiverById(Long id);

    Caregiver createCaregiver(Caregiver caregiver);

    Caregiver updateCaregiver(Caregiver caregiver);
}
