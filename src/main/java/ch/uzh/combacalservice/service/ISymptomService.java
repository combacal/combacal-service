package ch.uzh.combacalservice.service;

import ch.uzh.combacalservice.model.medicine.Symptom;
import org.springframework.stereotype.Service;

@Service
public interface ISymptomService {
    Symptom findSymptomById(Long id);

    Symptom createSymptom(Symptom symptom);

    Symptom updateSymptom(Symptom symptom);
}
