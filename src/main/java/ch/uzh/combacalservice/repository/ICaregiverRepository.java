package ch.uzh.combacalservice.repository;

import ch.uzh.combacalservice.model.people.Caregiver;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ICaregiverRepository extends PagingAndSortingRepository<Caregiver, Long> {
}
