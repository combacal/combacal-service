package ch.uzh.combacalservice.repository;

import ch.uzh.combacalservice.model.people.Client;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IClientRepository extends PagingAndSortingRepository<Client, Long> {
    List<Client> findByLastName(String lastName);
}
