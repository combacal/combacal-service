package ch.uzh.combacalservice.repository;

import ch.uzh.combacalservice.model.people.Community;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ICommunityRepository extends PagingAndSortingRepository<Community, Long> {
}
