package ch.uzh.combacalservice.repository;

import ch.uzh.combacalservice.model.shared.District;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IDistrictRepository extends PagingAndSortingRepository<District, Long> {
}
