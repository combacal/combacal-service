package ch.uzh.combacalservice.repository;

import ch.uzh.combacalservice.model.medicine.Treatment;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ITreatmentRepository extends PagingAndSortingRepository<Treatment, Long> {
}
