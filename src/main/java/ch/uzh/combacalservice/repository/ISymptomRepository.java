package ch.uzh.combacalservice.repository;

import ch.uzh.combacalservice.model.medicine.Symptom;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ISymptomRepository extends PagingAndSortingRepository<Symptom, Long> {
}
