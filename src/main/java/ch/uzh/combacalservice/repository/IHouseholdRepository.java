package ch.uzh.combacalservice.repository;

import ch.uzh.combacalservice.model.people.Household;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IHouseholdRepository extends PagingAndSortingRepository<Household, Long> {
    List<Household> findByLastName(String lastName);
}
