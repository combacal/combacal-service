package ch.uzh.combacalservice.repository;

import ch.uzh.combacalservice.model.medicine.Dossier;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IDossierRepository extends PagingAndSortingRepository<Dossier, Long> {
}
