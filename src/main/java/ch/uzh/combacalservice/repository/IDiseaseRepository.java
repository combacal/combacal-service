package ch.uzh.combacalservice.repository;

import ch.uzh.combacalservice.model.medicine.Disease;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IDiseaseRepository extends PagingAndSortingRepository<Disease, Long> {
}
