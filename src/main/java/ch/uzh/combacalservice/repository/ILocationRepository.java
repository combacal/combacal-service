package ch.uzh.combacalservice.repository;

import ch.uzh.combacalservice.model.shared.Location;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ILocationRepository extends PagingAndSortingRepository<Location, Long> {
}
