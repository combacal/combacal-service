package ch.uzh.combacalservice.repository;

import ch.uzh.combacalservice.model.admin.UserHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserHistoryRepository extends JpaRepository<UserHistory, Long> {
}
