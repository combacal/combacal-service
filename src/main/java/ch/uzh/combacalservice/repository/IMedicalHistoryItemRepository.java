package ch.uzh.combacalservice.repository;

import ch.uzh.combacalservice.model.medicine.MedicalHistoryItem;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IMedicalHistoryItemRepository extends PagingAndSortingRepository<MedicalHistoryItem, Long> {
}
