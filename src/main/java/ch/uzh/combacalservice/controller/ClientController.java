package ch.uzh.combacalservice.controller;


import ch.uzh.combacalservice.exception.ResourceNotFoundException;
import ch.uzh.combacalservice.model.people.Client;
import ch.uzh.combacalservice.model.people.dto.ClientDto;
import ch.uzh.combacalservice.service.IClientService;
import ch.uzh.combacalservice.util.mapper.MapStructMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/clients")
@Slf4j
@RequiredArgsConstructor
public class ClientController {
    private final IClientService clientService;
    private final MapStructMapper mapstructMapper;

    @GetMapping()
    public ResponseEntity<Iterable<ClientDto>> getAllClients() {
        log.info("GET: query all clients");
        Iterable<Client> foundClients = clientService.getAllClients();
        ArrayList<ClientDto> returnedClients = new ArrayList<>();
        foundClients.iterator().forEachRemaining(client -> returnedClients.add(mapstructMapper.domainToDto(client)));
        return new ResponseEntity<>(returnedClients, HttpStatus.OK);
    }

    @GetMapping("/{clientId}")
    public ResponseEntity<ClientDto> getClient(@PathVariable Long clientId) {
        log.info(String.format("GET: query client %d", clientId));
        Client foundClient = clientService.findClientById(clientId);
        if (null == foundClient) {
            throw new ResourceNotFoundException(Client.class, "id", clientId);
        }
        ClientDto returnedClient = mapstructMapper.domainToDto(foundClient);
        return new ResponseEntity<>(returnedClient, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<ClientDto> createClient(@RequestBody ClientDto newClientDto) {
        Client client = mapstructMapper.dtoToDomain(newClientDto);
        Client createdClient = clientService.createClient(client);
        log.info(String.format("POST: new client created with id %d", createdClient.getId()));
        ClientDto returnedClient = mapstructMapper.domainToDto(createdClient);
        return new ResponseEntity<>(returnedClient, HttpStatus.CREATED);
    }

    @PutMapping("/{clientId}")
    public ResponseEntity<ClientDto> updateClient(@PathVariable Long clientId, @RequestBody ClientDto clientDto) {
        log.info(String.format("PUT: updating client %d", clientId));
        Client client = mapstructMapper.dtoToDomain(clientDto);
        client.setId(clientId);
        Client updatedClient = clientService.updateClient(client);
        if (null == updatedClient) {
            throw new ResourceNotFoundException(Client.class, "id", clientId);
        }
        ClientDto returnedClient = mapstructMapper.domainToDto(updatedClient);
        return new ResponseEntity<>(returnedClient, HttpStatus.OK);
    }

    @DeleteMapping("/{clientId}")
    public ResponseEntity<ClientDto> deleteClient(@PathVariable Long clientId) {
        log.info(String.format("DELETE: deleting client %d", clientId));
        if (clientService.deleteClient(clientId)) {
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }
        log.error(String.format("Client with id %d could not be deleted!", clientId));
        Client persistedClient = clientService.findClientById(clientId);
        ClientDto returnedPersistedClient = mapstructMapper.domainToDto(persistedClient);
        return new ResponseEntity<>(returnedPersistedClient, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
