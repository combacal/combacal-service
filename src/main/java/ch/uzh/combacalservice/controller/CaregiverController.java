package ch.uzh.combacalservice.controller;


import ch.uzh.combacalservice.exception.ResourceNotFoundException;
import ch.uzh.combacalservice.model.people.Caregiver;
import ch.uzh.combacalservice.model.people.Client;
import ch.uzh.combacalservice.model.people.Community;
import ch.uzh.combacalservice.model.people.dto.CaregiverDto;
import ch.uzh.combacalservice.model.people.dto.ClientDto;
import ch.uzh.combacalservice.model.people.dto.CommunityDto;
import ch.uzh.combacalservice.service.ICaregiverService;
import ch.uzh.combacalservice.util.mapper.MapStructMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/caregivers")
@Slf4j
@RequiredArgsConstructor
public class CaregiverController {
    private final ICaregiverService caregiverService;
    private final MapStructMapper mapstructMapper;

    @GetMapping()
    public ResponseEntity<Iterable<CaregiverDto>> getAllCaregivers() {
        log.info("GET: query all caregivers");
        Iterable<Caregiver> foundCaregivers = caregiverService.getAllCaregivers();
        ArrayList<CaregiverDto> returnedCaregivers = new ArrayList<>();
        foundCaregivers.iterator().forEachRemaining(caregiver -> returnedCaregivers.add(mapstructMapper.domainToDto(caregiver)));
        return new ResponseEntity<>(returnedCaregivers, HttpStatus.OK);
    }

    @GetMapping("/{caregiverId}")
    public ResponseEntity<CaregiverDto> getCaregiver(@PathVariable Long caregiverId) {
        log.info(String.format("GET: query caregiver %d", caregiverId));
        Caregiver foundCaregiver = caregiverService.findCaregiverById(caregiverId);
        if (null == foundCaregiver) {
            throw new ResourceNotFoundException(Caregiver.class, "id", caregiverId);
        }
        CaregiverDto returnedCaregiver = mapstructMapper.domainToDto(foundCaregiver);
        return new ResponseEntity<>(returnedCaregiver, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<CaregiverDto> createCaregiver(@RequestBody CaregiverDto newCaregiverDto) {
        Caregiver newCaregiver = mapstructMapper.dtoToDomain(newCaregiverDto);
        Caregiver createdCaregiver = caregiverService.createCaregiver(newCaregiver);
        log.info(String.format("POST: new caregiver created with id %d", createdCaregiver.getId()));
        CaregiverDto returnedCaregiver = mapstructMapper.domainToDto(createdCaregiver);
        return new ResponseEntity<>(returnedCaregiver, HttpStatus.CREATED);
    }

    @PutMapping("/{caregiverId}")
    public ResponseEntity<CaregiverDto> updateCaregiver(@PathVariable Long caregiverId, @RequestBody CaregiverDto caregiverDto) {
        log.info(String.format("PUT: updating caregiver %d", caregiverId));
        Caregiver caregiver = mapstructMapper.dtoToDomain(caregiverDto);
        caregiver.setId(caregiverId);
        Caregiver updatedCaregiver = caregiverService.updateCaregiver(caregiver);
        if (null == updatedCaregiver) {
            throw new ResourceNotFoundException(Caregiver.class, "id", caregiverId);
        }
        CaregiverDto returnedCaregiver = mapstructMapper.domainToDto(caregiver);
        return new ResponseEntity<>(returnedCaregiver, HttpStatus.OK);
    }

    @DeleteMapping("/{caregiverId}")
    public ResponseEntity<String> deleteCaregiver(@PathVariable Long caregiverId) {
        log.info(String.format("deleting caregiver %d", caregiverId));
        return new ResponseEntity<>(String.format("deleting caregiver %d", caregiverId), HttpStatus.NO_CONTENT);
    }

    @GetMapping("/{caregiverId}/communities")
    public ResponseEntity<List<CommunityDto>> getCommunitiesForCaregiver(@PathVariable Long caregiverId) {
        log.info(String.format("GET: query communities for caregiver %d", caregiverId));
        Caregiver foundCaregiver = caregiverService.findCaregiverById(caregiverId);
        if (null == foundCaregiver) {
            throw new ResourceNotFoundException(Caregiver.class, "id", caregiverId);
        }
        List<Community> communities = foundCaregiver.getCommunities();
        List<CommunityDto> returnedCommunities = communities.stream().map(mapstructMapper::domainToDto).collect(Collectors.toList());
        return new ResponseEntity<>(returnedCommunities, HttpStatus.OK);
    }

    @GetMapping("/{caregiverId}/clients")
    public ResponseEntity<List<ClientDto>> getClientsForCaregiver(@PathVariable Long caregiverId) {
        log.info(String.format("GET: query clients for caregiver %d", caregiverId));
        Caregiver foundCaregiver = caregiverService.findCaregiverById(caregiverId);
        if (null == foundCaregiver) {
            throw new ResourceNotFoundException(Caregiver.class, "id", caregiverId);
        }
        List<Client> clients = foundCaregiver.getCommunities().stream().map(Community::getClients).flatMap(List::stream).collect(Collectors.toList());
        List<ClientDto> returnedClients = clients.stream().map(mapstructMapper::domainToDto).collect(Collectors.toList());
        return new ResponseEntity<>(returnedClients, HttpStatus.OK);
    }

    @GetMapping("/{caregiverId}/trainings")
    public ResponseEntity<String> getTrainingsForCaregiver(@PathVariable Long caregiverId) {
        log.info(String.format("query trainings for caregiver %d", caregiverId));
        return new ResponseEntity<>(String.format("query trainings for caregiver %d", caregiverId), HttpStatus.OK);
    }

    @GetMapping("/{caregiverId}/tasks")
    public ResponseEntity<String> getTasksForCaregiver(@PathVariable Long caregiverId) {
        log.info(String.format("query tasks for caregiver %d", caregiverId));
        return new ResponseEntity<>(String.format("query tasks for caregiver %d", caregiverId), HttpStatus.OK);
    }
}
