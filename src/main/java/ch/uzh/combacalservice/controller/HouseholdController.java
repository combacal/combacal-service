package ch.uzh.combacalservice.controller;


import ch.uzh.combacalservice.exception.ResourceNotFoundException;
import ch.uzh.combacalservice.model.people.Household;
import ch.uzh.combacalservice.model.people.dto.HouseholdDto;
import ch.uzh.combacalservice.service.IHouseholdService;
import ch.uzh.combacalservice.util.mapper.MapStructMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/households")
@Slf4j
@RequiredArgsConstructor
public class HouseholdController {
    private final IHouseholdService householdService;
    private final MapStructMapper mapStructMapper;

    @GetMapping()
    public ResponseEntity<Iterable<HouseholdDto>> getAllHouseholds() {
        log.info("GET: query all households");
        Iterable<Household> foundHouseholds = householdService.getAllHouseholds();
        ArrayList<HouseholdDto> returnedHouseholds = new ArrayList<>();
        foundHouseholds.iterator().forEachRemaining(household -> returnedHouseholds.add(mapStructMapper.domainToDto(household)));
        return new ResponseEntity<>(returnedHouseholds, HttpStatus.OK);
    }

    @GetMapping("/{householdId}")
    public ResponseEntity<HouseholdDto> getHousehold(@PathVariable Long householdId) {
        log.info(String.format("GET: query household %d", householdId));
        Household foundHousehold = householdService.findHouseholdById(householdId);
        if (null == foundHousehold) {
            throw new ResourceNotFoundException(Household.class, "id", householdId);
        }
        HouseholdDto returnedHousehold = mapStructMapper.domainToDto(foundHousehold);
        return new ResponseEntity<>(returnedHousehold, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<HouseholdDto> createHousehold(@RequestBody HouseholdDto newHousehold) {
        Household household = mapStructMapper.dtoToDomain(newHousehold);
        Household createdHousehold = householdService.createHousehold(household);
        log.info(String.format("POST: new household created with id %d", createdHousehold.getId()));
        HouseholdDto returnedHousehold = mapStructMapper.domainToDto(createdHousehold);
        return new ResponseEntity<>(returnedHousehold, HttpStatus.CREATED);
    }

    @PutMapping("/{householdId}")
    public ResponseEntity<HouseholdDto> updateHousehold(@PathVariable Long householdId, @RequestBody HouseholdDto householdDto) {
        log.info(String.format("PUT: updating household %d", householdId));
        Household household = mapStructMapper.dtoToDomain(householdDto);
        household.setId(householdId);
        Household updatedHousehold = householdService.updateHousehold(household);
        if (null == updatedHousehold) {
            throw new ResourceNotFoundException(Household.class, "id", householdId);
        }
        HouseholdDto returnedHousehold = mapStructMapper.domainToDto(updatedHousehold);
        return new ResponseEntity<>(returnedHousehold, HttpStatus.OK);
    }

    @DeleteMapping("/{householdId}")
    public ResponseEntity<HouseholdDto> deleteHousehold(@PathVariable Long householdId) {
        log.info(String.format("DELETE: deleting household %d", householdId));
        if (householdService.deleteHousehold(householdId)) {
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }
        log.error(String.format("household with id %d could not be deleted!", householdId));
        Household persistedHousehold = householdService.findHouseholdById(householdId);
        HouseholdDto returnedPersistedHousehold = mapStructMapper.domainToDto(persistedHousehold);
        return new ResponseEntity<>(returnedPersistedHousehold, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    // TODO: implement query endpoints for useful combined queries
    // build rest API to handle query parameters
    // http://localhost:8080/api/student/query?first=Uensal&last=Satan
//    @GetMapping("/query")
//    public ResponseEntity<Student> studentQueryParam(@RequestParam(name = "first") String firstName, @RequestParam(name = "last") String lastName) {
//        return new ResponseEntity<>(studentService.getStudentByLastNameAndFirstName(lastName, firstName), HttpStatus.OK);
//    }
}
