package ch.uzh.combacalservice.controller;

import ch.uzh.combacalservice.model.admin.dto.UserDto;
import ch.uzh.combacalservice.security.AuthenticationRequest;
import ch.uzh.combacalservice.security.UserDetailsImpl;
import ch.uzh.combacalservice.service.IAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin")
public class AdminController {

    final IAdminService adminService;

    @Autowired
    AdminController(IAdminService adminService) {
        this.adminService = adminService;
    }

    @GetMapping(path = "")
    public String admin() {
        return "admin test";
    }

    /**
     * Creates new User from input credentials. User gets USER access rights
     *
     * @param request contains needed information for creation
     * @return new user
     */
    @PostMapping(path = "/user")
    public UserDto createUser(@RequestBody AuthenticationRequest request) {
        UserDetailsImpl principal =
                (UserDetailsImpl) SecurityContextHolder
                        .getContext()
                        .getAuthentication().getPrincipal();
        return adminService.createUser(request, principal.getUsername());
    }

    /**
     * Creates new admin with ADMIN access rights
     *
     * @param request contains needed information for creation
     * @return new admin
     */
    @PostMapping(path = "/admin")
    public UserDto createAdmin(@RequestBody AuthenticationRequest request) {
        UserDetailsImpl principal =
                (UserDetailsImpl) SecurityContextHolder
                        .getContext()
                        .getAuthentication().getPrincipal();
        return adminService.createAdmin(request, principal.getUsername());
    }

    /**
     * Disables the user that is sent in the request.
     *
     * @param request Contains user information
     */
    @PutMapping(path = "/user/disable")
    public void disableUser(@RequestBody AuthenticationRequest request) {
        UserDetailsImpl principal =
                (UserDetailsImpl) SecurityContextHolder
                        .getContext()
                        .getAuthentication().getPrincipal();
        adminService.disableUser(request, principal.getUsername());
    }

    /**
     * Enables the user that is sent in the request.
     *
     * @param request Contains user information
     */
    @PutMapping(path = "/user/enable")
    public void enableUser(@RequestBody AuthenticationRequest request) {

        UserDetailsImpl principal =
                (UserDetailsImpl) SecurityContextHolder
                        .getContext()
                        .getAuthentication().getPrincipal();
        adminService.enableUser(request, principal.getUsername());
    }
}
