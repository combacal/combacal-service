package ch.uzh.combacalservice.controller;


import ch.uzh.combacalservice.exception.ResourceNotFoundException;
import ch.uzh.combacalservice.model.people.Community;
import ch.uzh.combacalservice.model.people.dto.CommunityDto;
import ch.uzh.combacalservice.service.ICommunityService;
import ch.uzh.combacalservice.util.mapper.MapStructMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/communities")
@Slf4j
@RequiredArgsConstructor
public class CommunityController {
    private final ICommunityService communityService;
    private final MapStructMapper mapstructMapper;

    @GetMapping()
    public ResponseEntity<Iterable<CommunityDto>> getAllCommunities() {
        log.info("GET: query all communities");
        Iterable<Community> foundCommunities = communityService.getAllCommunities();
        ArrayList<CommunityDto> returnedCommunities = new ArrayList<>();
        foundCommunities.iterator().forEachRemaining(community -> returnedCommunities.add(mapstructMapper.domainToDto(community)));
        return new ResponseEntity<>(returnedCommunities, HttpStatus.OK);
    }

    @GetMapping("/{communityId}")
    public ResponseEntity<CommunityDto> getCommunity(@PathVariable Long communityId) {
        log.info(String.format("GET: query community %d", communityId));
        Community foundCommunity = communityService.findCommunityById(communityId);
        if (null == foundCommunity) {
            throw new ResourceNotFoundException(Community.class, "id", communityId);
        }
        CommunityDto returnedCommunity = mapstructMapper.domainToDto(foundCommunity);
        return new ResponseEntity<>(returnedCommunity, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<CommunityDto> createCommunity(@RequestBody CommunityDto newCommunity) {
        Community community = mapstructMapper.dtoToDomain(newCommunity);
        Community createdCommunity = communityService.createCommunity(community);
        log.info(String.format("POST: new community created with id %d", createdCommunity.getId()));
        CommunityDto returnedCommunity = mapstructMapper.domainToDto(createdCommunity);
        return new ResponseEntity<>(returnedCommunity, HttpStatus.CREATED);
    }

    @PutMapping("/{communityId}")
    public ResponseEntity<CommunityDto> updateCommunity(@PathVariable Long communityId, @RequestBody Community community) {
        log.info(String.format("PUT: updating community %d", communityId));
        community.setId(communityId);
        Community updatedCommunity = communityService.updateCommunity(community);
        if (null == updatedCommunity) {
            throw new ResourceNotFoundException(Community.class, "id", communityId);
        }
        CommunityDto returnedCommunity = mapstructMapper.domainToDto(updatedCommunity);
        return new ResponseEntity<>(returnedCommunity, HttpStatus.OK);
    }

    @DeleteMapping("/{communityId}")
    public ResponseEntity<String> deleteCommunity(@PathVariable Long communityId) {
        log.info(String.format("DELETE: deleting community %d", communityId));
        return new ResponseEntity<>(String.format("DELETE: deleting community %d", communityId), HttpStatus.NO_CONTENT);
    }
}
