package ch.uzh.combacalservice.controller;

import ch.uzh.combacalservice.exception.CustomAuthenticationException;
import ch.uzh.combacalservice.security.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login")
public class LoginController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private CustomUserDetailsService userDetailsService;

    @Autowired
    private JwtUtils utils;

    /**
     * Endpoint for authentication of user that enables access to other endpoints
     *
     * @param request User information
     * @return Response containing token
     * @throws CustomAuthenticationException if authentication not successful
     */
    @PostMapping(path = "/authenticate")
    public AuthenticationResponse authenticate(@RequestBody AuthenticationRequest request) throws CustomAuthenticationException {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));
        } catch (BadCredentialsException e) {
            throw new CustomAuthenticationException("Incorrect username or password");
        }
        UserDetailsImpl details = userDetailsService.loadUserByUsername(request.getUsername());

        String jwt = utils.generateToken(details);

        return new AuthenticationResponse(jwt);

    }

}
