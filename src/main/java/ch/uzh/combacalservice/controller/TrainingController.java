package ch.uzh.combacalservice.controller;


import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/trainings")
@Slf4j
public class TrainingController {
    @GetMapping()
    public ResponseEntity<String> getAllTrainings() {
        log.info("query all trainings");
        return new ResponseEntity<>("query all trainings", HttpStatus.OK);
    }

    @GetMapping("/{trainingId}")
    public ResponseEntity<String> getTraining(@PathVariable Long trainingId) {
        log.info(String.format("query training %d", trainingId));
        return new ResponseEntity<>(String.format("query training %d", trainingId), HttpStatus.OK);
    }

    @GetMapping("/topic/{topic}")
    public ResponseEntity<String> getTrainingsForTopic(@PathVariable String topic) {
        log.info(String.format("query trainings for topic '%s'", topic));
        return new ResponseEntity<>(String.format("query trainings for topic '%s'", topic), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<String> createTraining() {
        log.info("new training created");
        return new ResponseEntity<>("new training created", HttpStatus.CREATED);
    }

    @PutMapping("/{trainingId}")
    public ResponseEntity<String> updateTraining(@PathVariable Long trainingId) {
        log.info(String.format("updating training %d", trainingId));
        return new ResponseEntity<>(String.format("updating training %d", trainingId), HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{trainingId}")
    public ResponseEntity<String> deleteTraining(@PathVariable Long trainingId) {
        log.info(String.format("deleting training %d", trainingId));
        return new ResponseEntity<>(String.format("deleting training %d", trainingId), HttpStatus.NO_CONTENT);
    }
}
