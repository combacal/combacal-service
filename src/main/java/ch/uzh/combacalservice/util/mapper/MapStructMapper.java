package ch.uzh.combacalservice.util.mapper;

import ch.uzh.combacalservice.model.medicine.*;
import ch.uzh.combacalservice.model.medicine.dto.*;
import ch.uzh.combacalservice.model.people.Caregiver;
import ch.uzh.combacalservice.model.people.Client;
import ch.uzh.combacalservice.model.people.Community;
import ch.uzh.combacalservice.model.people.Household;
import ch.uzh.combacalservice.model.people.dto.CaregiverDto;
import ch.uzh.combacalservice.model.people.dto.ClientDto;
import ch.uzh.combacalservice.model.people.dto.CommunityDto;
import ch.uzh.combacalservice.model.people.dto.HouseholdDto;
import ch.uzh.combacalservice.model.shared.District;
import ch.uzh.combacalservice.model.shared.Location;
import ch.uzh.combacalservice.model.shared.dto.DistrictDto;
import ch.uzh.combacalservice.model.shared.dto.LocationDto;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.stream.Collectors;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE,  injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface MapStructMapper {
    // Package model.medicine
    DiseaseDto domainToDto(Disease disease);
    Disease dtoToDomain(DiseaseDto diseasedto);
    DossierDto domainToDto(Dossier dossier);
    Dossier dtoToDomain(DossierDto dossierDto);
    MedicalHistoryItemDto domainToDto(MedicalHistoryItem medicalHistoryItem);
    MedicalHistoryItem dtoToDomain(MedicalHistoryItemDto medicalHistoryItemDto);
    SymptomDto domainToDto(Symptom symptom);
    Symptom dtoToDomain(SymptomDto symptomDto);
    TreatmentDto domainToDto(Treatment treatment);
    Treatment dtoToDomain(TreatmentDto treatmentDto);

    // Package model.people
    CaregiverDto domainToDto(Caregiver caregiver);
    Caregiver dtoToDomain(CaregiverDto caregiverDto);
    default Client dtoToDomain(ClientDto clientDto) {
        Client newClient = new Client();
        newClient.setFirstName(clientDto.getFirstName());
        newClient.setLastName(clientDto.getLastName());
        newClient.setPhoneNumber(clientDto.getPhoneNumber());
        newClient.setGender(clientDto.getGender());
        newClient.setBirthDate(clientDto.getBirthDate());
        newClient.setMaritalStatus(clientDto.getMaritalStatus());
        newClient.setParents(clientDto.getParents().stream().map(this::dtoToDomain).collect(Collectors.toList()));
        newClient.setHousehold(dtoToDomain(clientDto.getHousehold()));
        newClient.setDiseases(clientDto.getDiseases().stream().map(this::dtoToDomain).collect(Collectors.toList()));
        newClient.setMedicalDossier(dtoToDomain(clientDto.getMedicalDossier()));
        return newClient;
    }
    default ClientDto domainToDto(Client client) {
        ClientDto clientDto = new ClientDto();
        clientDto.setFirstName(client.getFirstName());
        clientDto.setLastName(client.getLastName());
        clientDto.setPhoneNumber(client.getPhoneNumber());
        clientDto.setGender(client.getGender());
        clientDto.setBirthDate(client.getBirthDate());
        clientDto.setMaritalStatus(client.getMaritalStatus());
        clientDto.setParents(client.getParents().stream().map(this::domainToDto).collect(Collectors.toList()));
        clientDto.setHousehold(domainToDto(client.getHousehold()));
        clientDto.setDiseases(client.getDiseases().stream().map(this::domainToDto).collect(Collectors.toList()));
        clientDto.setMedicalDossier(domainToDto(client.getMedicalDossier()));
        return clientDto;
    }
    CommunityDto domainToDto(Community community);
    Community dtoToDomain(CommunityDto communityDto);
    HouseholdDto domainToDto(Household household);
    Household dtoToDomain(HouseholdDto householdDto);

    //Package model.shared
    DistrictDto domainToDto(District district);
    District dtoToDomain(DistrictDto districtDto);
    LocationDto domainToDto(Location location);
    Location dtoToDomain(LocationDto locationDto);
}
